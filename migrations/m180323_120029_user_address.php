<?php

namespace app\migrations;
use app\commands\Migration;

class m180323_120029_user_address extends Migration
{
   public function getTableName()
    {
        return 'user_address';
    }

    public function getForeignKeyFields()
    {
        return [
            'user_id' => ['mub_user','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
        'user_id'  =>  'user_id',
        'address' => 'address'];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'address' => $this->string()->notNull(),
            'alt_address' => $this->string(),
            'city' => $this->string()->notNull(),
            'state' => $this->string()->notNull(),
            'pincode' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'status' => "enum('Active','Inactive') NOT NULL DEFAULT 'Active'",
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'",
        ];
    }
}
