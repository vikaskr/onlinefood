<?php

namespace app\migrations;
use app\commands\Migration;

class m180522_101305_offers extends Migration
{
    public function getTableName()
    {
        return 'offers';
    }
    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'restaurant_id' => ['restaurant', 'id']
        ];
    }

    public function getKeyFields()
    {
        return [
            'offer_text' => 'offer_text',
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'restaurant_id' => $this->integer()->notNull(),
            'offer_text' => $this->string()->notNull(),
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'inactive'",
            'icon_url' => $this->string(),
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}

