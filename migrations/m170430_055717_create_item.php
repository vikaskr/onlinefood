<?php

namespace app\migrations;
use app\commands\Migration;

class m170430_055717_create_item extends Migration
{
    public function getTableName()
    {
        return 'item';
    }

    public function getForeignKeyFields()
    {
        return [
            'mub_user_id' => ['mub_user', 'id'],
            'menu_id' => ['menu','id'],
        ];
    }

    public function getKeyFields()
    {
        return [
            'item_name' => 'item_name',
            'price' => 'price'
        ];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'mub_user_id' => $this->integer()->notNull(),
            'menu_id' => $this->integer()->notNull(),
            'item_name' => $this->string()->notNull(),
            'item_slug' => $this->string()->notNull(),
            'plate' => "enum('full-plate','half-plate') NOT NULL DEFAULT 'full-plate'",
            'food_type' => "enum('veg','non-veg') NOT NULL DEFAULT 'veg'",
            'price' => $this->integer(),
            'sell_price' => $this->integer(),
            'discount_price' => $this->integer(),
            'image_url' => $this->string(),
            'description' => $this->text(),
            'feature' => $this->boolean()->defaultValue('0'),
            'recomended' => $this->boolean()->defaultValue('0'), 
            'status' => "enum('active','inactive') NOT NULL DEFAULT 'active'",
            'created_at' => $this->dateTime()->defaultValue('1970-01-01 12:00:00'),
            'updated_at' => $this->dateTime(),
            'del_status' => "enum('0','1') NOT NULL COMMENT '0-Active,1-Deleted DEFAULT 0' DEFAULT '0'"
        ];
    }
}
