<?php

namespace app\migrations;
use app\commands\Migration;

class m180223_052947_resturant_item extends Migration
{
    public function getTableName()
    {
        return 'resturant_item';
    }

    public function getForeignKeyFields()
    {
        return [
            'item_id' => ['item','id'],
            'resturant_id' => ['restaurant','id'],
        ];
    }

    public function getKeyFields()
    {
        return [];
    }

    public function getFields()
    {
        return [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer()->notNull(),
            'resturant_id' => $this->integer()->notNull(),
        ];
    }
}
