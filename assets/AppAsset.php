<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/css/bootstrap.min.css',
        // '/css/font-awesome.min.css',
        '/css/animsition.min.css',
        '/css/animate.css',
        '/css/style.css',
        '/css/jquery.typeahead.css',

    ];
    public $js = [
        'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCFvp1Sp5fGUIp0XFfTW5yV_66BN2-L2qs',
        '/js/custom_mub_frontend.js',
        '/js/jquery.typeahead.js',
        '/js/tether.min.js',
        // '/js/bootstrap.min.js',
        '/js/animsition.min.js',
        '/js/bootstrap-slider.min.js',
        '/js/jquery.isotope.min.js',
        '/js/headroom.js',
        '/js/foodpicky.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'app\assets\FontAwesomeAsset'
    ];
}
