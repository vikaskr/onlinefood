<?php 

namespace common\helpers;

class MailHelper
{
	public static function sendMail($from,$to,$subject,$layout,$formvars)
	{
		\Yii::$app->mailer->compose()
		->setFrom($from)
		->setTo($to)
		->setSubject($subject)
		if(!is_file($layout)){
			->setHtmlBody($layout)	
		}
	}

}