<?php
/**
 * @copyright Copyright (c) 2015 Yiister
 * @license https://github.com/yiister/yii2-gentelella/blob/master/LICENSE
 * @link http://gentelella.yiister.ru
 */

namespace yiister\gentelella\assets;

class Asset extends \yii\web\AssetBundle
{
	public $js = [
	'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key=AIzaSyCFvp1Sp5fGUIp0XFfTW5yV_66BN2-L2qs'
	];
	
    public $depends = [
        'yiister\gentelella\assets\ThemeAsset',
        'yiister\gentelella\assets\ExtensionAsset',
    ];
}
