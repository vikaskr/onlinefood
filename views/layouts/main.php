<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\ActiveForm;
use app\assets\AppAsset;
use app\models\MubUser;
use app\modules\MubAdmin\modules\hotels\models\Item;

AppAsset::register($this);

 $this->registerMetaTag([
      'title' => 'og:title',
      'content' =>'OSMSTAYS Property', 
   ]);

   $this->registerMetaTag([
      'app_id' => 'fb:app_id',
      'content' => '892362904261944'
   ]);

   $this->registerMetaTag([
      'type' => 'og:type',
      'content' => 'article'
   ]);

   $this->registerMetaTag([
      'url' => 'og:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
      $this->registerMetaTag([
         'image' => 'og:image',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo1.jpg',
      ]);
   $this->registerMetaTag([
      'description' => 'og:description',
      'content' => 'Looking for a house on rent in Gurgaon? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.'
   ]);

/*close facebook meta tag*/

/*twitter meta tag*/

   $this->registerMetaTag([
      'card' => 'twitter:card',
      'content' => "summury"
   ]);
   $this->registerMetaTag([
      'site' => "twitter:site",
      'content' => "@publisher_handle"
   ]);
   
   $this->registerMetaTag([
      'title' => 'twitter:title',
      'content' => 'OSMSTAYS Property'
   ]);

   $this->registerMetaTag([
      'description' => 'twitter:description',
      'content' => 'Looking for a house on rent in Gurgaon? OSMSTAYS provide fully furnished/semi-furnished flats at an Affordable rate with no brokerage and no lock-in period.' 
   ]);

   $this->registerMetaTag([
      'creater' => 'twitter:creater',
      'content' => '@author_handle' 
   ]);

   $this->registerMetaTag([
      'url' => 'twitter:url',
      'content' => 'http://'.$_SERVER['HTTP_HOST'].\Yii::$app->request->url,
   ]);
   $this->registerMetaTag([
         'image' => 'twitter:image:src',
         'content' => 'http://'.$_SERVER['HTTP_HOST'].'/uploads/logo1.jpg',
      ]);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link href='//fonts.googleapis.com/css?family=Josefin+Sans:400,700italic,700,600italic,600,400italic,300italic,300,100italic,100' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <style type="text/css">
    .navbar-light .navbar-toggler {
     background-image: none!important;
      }
    </style>
</head>
<script async src="http://www.googletagmanager.com/gtag/js?id=UA-104726724-1"></script>
<script>

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-104726724-1');
</script>

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "@id":"#organization",
  "name" : "osmstays.com",
  "url": "http://osmstays.com",
"logo": "http://osmstays.com/images/logo.jpg",
  "sameAs" : [
      "http://www.facebook.com/osmstays/",
      "http://twitter.com/osmstays",
      "http://plus.google.com/u/0/102988358230820191177",
      "http://www.linkedin.com/company/osmstays/",
      "http://www.instagram.com/osmstays/",
      "http://osmstays.tumblr.com/"
      ]
  }
  </script>


<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "WebSite",
  "@id":"#website",
  "url": "http://osmstays.com/",
  "name":"osmstays.com"  
  }
}
</script>

<body  class="home">
<?php $this->beginBody() ?>
<div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
        <!--header starts-->
        <header id="header" class="header-scroll top-header headrom">
            <!-- .navbar -->
            <nav class="navbar navbar-light">
                <div class="container">
                    <button class="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse" data-target="#mainNavbarCollapse">&#9776;</button>
                    <a class="navbar-brand" href="/"> <img class="img-rounded" src="/images/mexpress.png" alt=""> </a>
                    <div class="collapse navbar-toggleable-md  float-lg-right" id="mainNavbarCollapse">
                        <ul class="nav navbar-nav">
                            <li class="nav-item"> <a class="nav-link active" href="/" style="margin-top: 14px;">Home <span class="sr-only">(current)</span></a> </li>
                            <li class="nav-item dropdown">
                              <a class="nav-link" href="/site/allrestaurant/" style="margin-top: 14px; color: #000;">All Restaurants</a>
                            </li>
                            <li class="nav-item dropdown" style="margin-top: 7px;">
                              <a id="help" data-toggle="modal" data-target="#myModal" style=" color: #000;"> Help</a>
                            </li>
                        <?php if(\Yii::$app->user->id==null){?>
                    <li class="nav-item" style="margin-top: 7px;"><a href="/site/client-login" id="loginmodal" data-toggle="modal" data-target="#myModal" class="bgd">Login | Register</a></li>
                        <?php }?>
                            <?php if(!\Yii::$app->user->id==null){
                               $mubUserId = \app\models\User::getMubUserId();
                                $currentUser = MubUser::findOne($mubUserId);
                                ?>
                                <li class="nav-item dropdown" style="margin-top: 7px;">
                                <a class="dropdown-toggle bgd" data-toggle="dropdown" role="button"><?=$currentUser->first_name;?></a>
                                <ul class="dropdown-menu" style="margin-top: 0px">
                                    <li><a href="/site/profile">View Profile</a></li>
                                   <!--  <li><a href="/site/history">History</a></li> -->
                                    <li><a href="<?= Url::to(['/site/logout'])?>" data-method="post" >Logout</a></li>
                                </ul>
                            </li>
                        <?php }?>
                            <li class="nav-item">
                              <a href="/site/checkout"><i class="fa fa-shopping-cart" aria-hidden="true" style="margin-top: 9px;font-size: 25px;"></i><span class="cartcount"><?php echo $this->render('/site/cartcount');?></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- /.navbar -->
        </header>
  </div>
<?= $content ?>
        <footer class="footer">
            <div class="container">
                <!-- top footer statrs -->
                <div class="row top-footer">
                    <div class="col-xs-12 col-sm-3 footer-logo-block color-gray">
                        <a href="#"> <img src="/images/mexpress.png" alt="Footer logo"> </a> <span>Order Delivery &amp; Take-Out </span> </div>
                    <div class="col-xs-12 col-sm-2 about color-gray">
                        <h5>About Us</h5>
                        <ul>
                            <li><a href="#">About us</a> </li>
                            <li><a href="/site/term">Terms & Condition</a> </li>
                            <li><a href="/site/privacy">Privacy Policy</a> </li>
                            <li><a href="/site/return">Return Policy</a> </li>
                            <li> <a id="help" data-toggle="modal" data-target="#myModal" style="color: #fff; font-size: 16px; cursor: pointer;"> Contact</a </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-2 how-it-works-links color-gray">
                        <h5>How it Works</h5>
                        <ul>
                            <li><a href="#">Enter your location</a> </li>
                            <li><a href="#">Choose restaurant</a> </li>
                            <li><a href="#">Choose meal</a> </li>
                            <li><a href="#">Pay via credit card</a> </li>
                            <li><a href="#">Wait for delivery</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-2 pages color-gray">
                        <h5>Pages</h5>
                        <ul>
                            <li><a href="#">Search results page</a> </li>
                            <li><a href="#">User Sing Up Page</a> </li>
                            <li><a href="#">Pricing page</a> </li>
                            <li><a href="#">Make order</a> </li>
                            <li><a href="#">Add to cart</a> </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-3 popular-locations color-gray">
                        <h5>Popular locations</h5>
                        <ul>
                            <li><a href="#">Baijnath</a> </li>
                            <li><a href="#">Paprola</a> </li>
                             <li><a href="#">Bir</a> </li>
                            <li><a href="#">Palampur</a> </li>
                        </ul>
                    </div>
                </div>
                <!-- top footer ends -->
                <!-- bottom footer statrs -->
                <div class="bottom-footer">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3 payment-options color-gray">
                            <h5>Payment Options</h5>
                            <ul>
                                <li>
                                    <a href="#"> <img src="/images/payu.jpg" alt="Paypal"> </a>
                                </li>
                                <li>
                                    <a href="#"> <img src="/images/mastercard.png" alt="Mastercard"> </a>
                                </li>
                                <li>
                                    <a href="#"> <img src="/images/maestro.png" alt="Maestro"> </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-4 address color-gray">
                            <h5>Phone: <a href="tel:8988224444">8988224444</a></h5> </div>
                        <div class="col-xs-12 col-sm-5 additional-info color-gray">
                            <h5>Addition informations</h5>
                            <p>Join the thousands of other restaurants who benefit from having their menus on TakeOff</p>
                        </div>
                    </div>
                </div>
                <!-- bottom footer ends -->
            </div>
        </footer>
        <!-- end:Footer -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-info" id="dynamic-modal">
                    
                    
                </div>
            </div>
        </div>
</body>
<?php $this->endBody();?>
</html>
<?php $this->endPage();?>
