<?php 

$menu = [];
if(\Yii::$app->user->can('dashboard/index'))
{
    $menu[] = ["label" => "Dashboard", "url" => "/mub-admin", "icon" => "home"];        
}

if(\Yii::$app->user->can('album/index'))
{
$menu[] = [
            "label" => "Gallery", 
            "url" => ["/mub-admin/gallery"], 
            "icon" => "picture-o"
        ];
}
if(\Yii::$app->user->can('user/index'))
{
     $userss = new  app\models\MubUser();

    $newCount = $userss::find()->where(['del_status' => '0','status' => 'inactive'])->count();
    $userssCount = $userss::find()->where(['del_status' => '0','status' => 'active'])->count();
    $menu[] = ["label" => "Users", "url" => ["/mub-admin/users"], "icon" => "user",
                "badge" => ($newCount > 0) ? $newCount : $userssCount,
                "badgeOptions" => ["class" => ($newCount > 0) ?"label-success" : ''],
                "icon" => "cog"
            ];
}

if(\Yii::$app->user->can('user/index'))
{
$menu[] = ["label" => "Front-end-Users", "url" => ["/mub-admin/users/user/frontend-user"], "icon" => "user"];
}

if(\Yii::$app->user->can('restaurant/index'))
{
    $mubUserId = \app\models\User::getMubUserId();
    $userRole = \Yii::$app->controller->getUserRole();
    if($userRole == 'admin')
    {
    $states = new \app\models\State();
    $active = $states::find()->where(['active' => '1'])->all();
    $submenu = [];
    $restaurant = new \app\modules\MubAdmin\modules\hotels\models\Restaurant();

    foreach ($active as $key => $activeState) {
        $inactiveProp = intval($restaurant::find()->where(['state_id' => $activeState->id,'status' => 'inactive'])->count());
        $activeProp = intval($restaurant::find()->where(['state_id' => $activeState->id,'status' => 'active'])->count());
        $submenu[] =[
          "label" => $activeState->state_name,
          "url" => "/mub-admin/hotels/restaurant?state=".$activeState->id,
          'ptions'=> ['class'=>'displayblock'],
          "badge" => ($inactiveProp > 0) ? $inactiveProp : $activeProp,
          "badgeOptions" => ["class" => ($inactiveProp > 0) ? "label-success" : '']
          ];
        }
    }
    else
    {
    $restaurant = new \app\modules\MubAdmin\modules\Hotels\models\Restaurant();
    $userStates = new \app\models\UserStates();
    $user = new \app\models\User();
    $mubUserId = $user::getMubUserId();
    $states = new \app\models\State();
    $mubUserStates = $userStates::find()->where(['mub_user_id' => $mubUserId])->all();
    $submenu = [];
    foreach ($mubUserStates as $key => $activeState) {
        $currentState = $states::findOne($activeState->state_id);
         $inactiveProp = \intval($restaurant::find()->where(['state_id' => $activeState->id,'mub_user_id' => $mubUserId,'status' => 'inactive'])->count());
        $activeProp = \intval($restaurant::find()->where(['state_id' => $activeState->id,'mub_user_id' => $mubUserId,'status' => 'active'])->count());
        $submenu[] =[
          "label" => $currentState->state_name,
          "url" => "/mub-admin/hotels/restaurant?state=".$currentState->id,
          "badge" => ($inactiveProp > 0) ? $inactiveProp : $activeProp,
          "badgeOptions" => ["class" => ($inactiveProp > 0) ? "label-success" : '']
          ];
        }
    }
    if(\Yii::$app->user->can('service/index'))
    {
        $menu[] = ["label" => "Services", "url" => "/mub-admin/hotels/service", "icon" => "fa fa-cogs"];        
    }

    if(\Yii::$app->user->can('offers/index'))
    {
        $menu[] = ["label" => "Offers", "url" => "/mub-admin/hotels/offers", "icon" => "fa fa-cogs"];        
    }

$menu[] = [
        "label" => "Restaurant",
        "url" => "#",
        "icon" => "home",
        'options'=> ['class'=>'displayblock'],
        "items" => [
            [
                "label" => "States",
                "url" => "#",
                "items" => $submenu
            ],
        ],];

}
if(\Yii::$app->user->can('restaurant/index'))
{
    $menu[] = ["label" => "Report", "url" => "/mub-admin/hotels/restaurant/report", "icon" => "fa fa-cogs"];        
}


?>
