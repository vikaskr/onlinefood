<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css">.modal-header {
    padding: 15px;
    border-bottom: 1px solid #fff;</style>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>                        
</div>
<div class="modal-body real-spa">
<div class="login-grids">
    <div class="login">
        <div class="login-right">
            <?php $form = ActiveForm::begin(['layout' => 'horizontal','enableAjaxValidation' => true,'validationUrl' => ['site/client-register-validate'],'options' => ['id' => 'frontend-signup','method' => 'POST','data-pjax' => true],'action' => ['/#']]); ?>
                <div class="col-md-12 text-center" style="margin-top: -2em"><h2 style="margin-bottom: 1em;">Register</h2></div>
                <div class="row"><?= $form->field($model, 'first_name')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'last_name')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'username')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'password')->passwordInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'mobile')->textInput(['class' => 'form-control','maxlength' => 10,]);?></div>
                <div class="row"><?= $form->field($model, 'email')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'address')->textInput(['class' => 'form-control', 'id' => 'searchPlaces']);?></div>
                <!-- <div class="col-md-3"></div> --><div class="col-md-9"><input type="submit" value="Register Now" style="margin-top: 1em!important;color: #fff; margin-bottom: 0.2em; width: 150px; background: #058a0b; padding: 5px;">&nbsp;&nbsp; Already Registered | Go to 
                <a href="#" class="signup-user" id="signin-modal" style="color: red; font-weight: 600; font-size: 17px;">Login</a></div>
             <?php ActiveForm::end(); ?>
        </div>
        <div class="clearfix"></div>                                
    </div>
    <p style="color: #000!important;">By logging in you agree to our <a href="#" style="color: #6298c7!important;" target="_blank">Terms and Conditions</a> and <a href="#" style="color: #6298c7!important;" target="_blank">Privacy Policy</a></p>
</div>
</div>

