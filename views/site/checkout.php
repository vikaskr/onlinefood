<?php
            
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    use yii\widgets\ActiveForm;
    use yz\shoppingcart\ShoppingCart;
    $user = new \app\models\MubUser();
    $cart = new ShoppingCart();
    $cartItems = $cart->getPositions();

    $restaurantImg = new \app\modules\MubAdmin\modules\hotels\models\RestaurantImages();
    $count = count ( $cartItems );
    if(!empty($cartItems)){
    $restaurant = current($cartItems)->menu->restaurant;
   
    $resId = $restaurant->id;

    $images = $restaurantImg::find()->where(['restaurant_id' => $restaurant->id,'del_status' => '0'])->all();
    
       foreach($cartItems as $value){
                     $itemCartId = $value['id'];
                     $itemCartName = $value['item_name'];
                     $quant = $value->getQuantity();
                   }

              $time_now = mktime(date('h')+5,date('i')+30);
              $localTime = date("Y-m-d  H:i:s",$time_now);

?>
<style type="text/css">
@media (max-width: 680px) {
.pad{
  padding: 20px!important;
}
}
input{
    line-height: inherit;
    width: 100%;}
</style>

<section class="featured-restaurants" style="background-color: #c5c5c5!important;">
  <div class="container" style="min-height: 800px;">
   <div class="row">
    <div class="col-md-5" style="min-height: 800px;">
      <div class="col-xs-12 col-sm-12 col-md-12 single-restaurant grill thaifood pasta pizza">
                    <div class="restaurant-wrap pad">
                        <div class="row"><div class="col-md-5"></div>
                            <div class="col-md-6">
               <h4 style="margin-bottom: 10px; margin-left: -10px;"><br/><b>Payments</b></h4></div><br/>
                    
                        <?php
                         if(!Yii::$app->user->isGuest){
                         $mubUserId = \app\models\User::getMubUserId();
                         $mubUserModel = new \app\models\MubUser();
                         $currentUser = $mubUserModel::findOne($mubUserId);
                         $userDetails = $currentUser;
                         $contactDetails = $currentUser->mubUserContacts;
                         
                          if(!empty($userDistance)){
                         $userLat = $contactDetails->lat;
                         $userLong = $contactDetails->long;
                         $reslat = $restaurant->lat;
                         $reslong = $restaurant->long;

                          $theta = $userLong - $reslong;
                          $dist = sin(deg2rad($userLat)) * sin(deg2rad($reslat)) +  cos(deg2rad($userLat)) * cos(deg2rad($reslat)) * cos(deg2rad($theta));
                          $dist = acos($dist);
                          $dist = rad2deg($dist);
                          $miles = $dist * 60 * 1.1515;
                          $userDistance = ($miles * 1.609344);
                          }
                          $tokenAmt = ($cart->getCost())+($restaurant->shiping_charge); 
                                        // p($amount);
                          // Merchant key here as provided by Payu
                          //payu test key : rjQUPktU , Salt: e5iIg1jwi8
                          $MERCHANT_KEY = "bstnPW9E";

                          // Merchant Salt as provided by Payu
                          $SALT = "AJi6ZxOUP2";

                          // End point - change to https://secure.payu.in for LIVE mode
                          $PAYU_BASE_URL = "https://secure.payu.in";

                          $formError = 0;
                          $action = $PAYU_BASE_URL . '/_payment';

                        $form = ActiveForm::begin(['options' => ['id' => 'confirm-payment','name' => 'payuForm','method' => 'POST'],'action' => $action]);
                         $booking = new \app\models\Booking();
                         ?>
                                 
                                  <input type="hidden" id="key" name="key" value="<?=$MERCHANT_KEY;?>">
                                  <input type="hidden" id="price" name="amount" value="<?= $tokenAmt;?>">
                                  <input type="hidden" name="txnid" value="<?=$currentUser->username.'_'.time();?>" />
                                  <input type="hidden" id="item" name="item" value="<?= isset($itemCartName)? $itemCartName: '';?>">
                                  <input type="hidden" id="quant" name="quant" value="<?= isset($quant)? $quant: '';?>">
                                  <input type="hidden" id="firstname" name="firstname" value="<?= $currentUser->first_name?>">
                                  <input type="hidden" id="email" name="email" value="<?=$contactDetails->email;?>">
                                  <input type="hidden" id="phone" name="phone" value="<?=$contactDetails->mobile;?>">
                                  <input type="hidden" id="delivery_charge" name="delivery_charge" value="">
                                  <input type="hidden" id="resname" name="resname" value="<?= isset($restaurant->restaurant_name)? $restaurant->restaurant_name: '';?>">
                                  <input type="hidden" id="productinfo" name="productinfo" value="restaurant">
                                  <input type="hidden" id="lastname" name="lastname" value="<?= $currentUser->last_name?>">
                                  
                                  <input type="hidden" id="address2" name="address2" value="<?= isset($restaurant->sa_b)? $restaurant->sa_b: '';?>">
                                  <input type="hidden" id="city" name="city" value="<?= isset($restaurant->city_name)? $restaurant->city_name: '';?>">

                                   <input type="hidden" id="address" name="address" value="<?= isset($contactDetails->address)? $contactDetails->address: '';?>">

                                   <input type="hidden" id="time" name="time" value="<?= $localTime;?>">

                                  <input type="hidden" id="state" name="state" value="<?= isset($restaurant->state->state_name)? $restaurant->state->state_name: '';?>">
                                  <input type="hidden" id="surl" name="surl" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/payu/success.php';?>">
                                  <input type="hidden" id="furl" name="furl" value="<?='http://'.$_SERVER['SERVER_NAME'].'/components/payu/failure.php';?>">
                                  <input type="hidden" id="udf1" name="udf1" value="">
                                  <input type="hidden" id="udf2" name="udf2" value="">
                                  <input type="hidden" id="udf3" name="udf3" value="">
                                  <input type="hidden" id="udf4" name="udf4" value="">
                                  <input type="hidden" id="udf5" name="udf5" value="">
                                  <input type="hidden" id="udf6" name="udf6" value="">
                                  <input type="hidden" id="udf7" name="udf7" value="">
                                  <input type="hidden" id="udf8" name="udf8" value="">
                                  <input type="hidden" id="udf9" name="udf9" value="">
                                  <input type="hidden" id="udf10" name="udf10" value="">
                                  <input type="hidden" id="service_provider" name="service_provider" value="payu_paisa">
                                 <br /><br />
                                 
                                <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-9">
                                  <p>Delivery Address : <b><?= $contactDetails->street_address.' , '.$contactDetails->address;?></b></p>
                                </div><br/>
                              </div>

                              <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8"><p style="text-align: center;">or</p>
                                  <?= $form->field($booking, 'alt_address')->textInput(['name' => 'address3','placeholder' => 'House No. (examples)','id' => 'address3','rows' => '4'])->label('Other Delivery Address');?>
                                  <?= $form->field($booking, 'street_address')->textInput(['name' => 'searchPlaces', 'class' => 'searchPlaces' ,'id' => 'searchPlaces','rows' => '4'])->label('Street Address');?>
                                 <?= $form->field($booking,'lat')->hiddenInput(['id' => 'lat','name' => 'lat',])->label(false);?>
                                 <?= $form->field($booking,'long')->hiddenInput(['id' => 'long','name' => 'long',])->label(false);?>
                                 <?= $form->field($restaurant,'lat')->hiddenInput(['id' => 'reslat','value' => $restaurant->lat,'name' => 'reslat',])->label(false);?>
                                 <?= $form->field($restaurant,'long')->hiddenInput(['id' => 'reslong','value' => $restaurant->long,'name' => 'reslong',])->label(false);?>
                                 <?= $form->field($restaurant,'shiping_charge')->hiddenInput(['id' => 'ship','value' => $restaurant->shiping_charge,'name' => 'reslat',])->label(false);?>
                                 <?= $form->field($restaurant,'shiping_charge_above')->hiddenInput(['id' => 'ship2','value' => $restaurant->shiping_charge_above,'name' => 'reslong',])->label(false);?>
                                 <input type="hidden" name="totalprice" id="totalprice" value="<?= $cart->getCost();?>">

                                </div><br/></div>

                                 <div class="row">
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                    <input type="submit" value="Payment" formtarget="_blank" class="newd">
                                    <input type="button" value="Cash On Delivery" id="codelivery" class="newd2" onClick="this.disabled=true; this.value='Processing.....';"><br/>
                                    <!-- <input type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" > -->
                                    </div>
                                </div>
                                  <br/> <br/>
                            <?php ActiveForm::end(); }?>
                            <?php if(Yii::$app->user->isGuest){?>
                              <br/>
                              <br/>
                              <div class="row">
                             <div class="col-md-5"></div><div class="col-md-5"><a href="/site/client-login" id="loginmodal" data-toggle="modal" data-target="#myModal" class="bgd button">Login | Register</a></div></div>
                             <?php }?>
                        </div>
                    </div>
                </div>
                </div>

                <div class="col-md-6" style="background-color: #fff;"><br/>
                  <?php 
                    if(isset($images))
                    {
                      $logoImage = $images[0]->thumbnail_url;
                      $res_img = ($images[0]->thumbnail_url) ? $images[0]->thumbnail_url : 'NA';
                    }
                    else
                    {
                        $res_img = 'NA';
                    }
                 ?><div class="row">
                   <div class="col-md-4">
                      <figure><img src="<?= $res_img;?>" alt="Profile Image" style="height: 88px!important; width: 140px!important;">
                      </figure>
                  </div>
                  <div class="col-md-7" style="border-bottom: 2px solid #000;">
                     <h3><b><?= $restaurant->restaurant_name; ?></b></h3>
                     <?= $restaurant->sa_a.', '.$restaurant->sa_b; ?>
                   </div>
                  </div> <br/><br/>
                  <div class="row">
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                          <div class="row" style="background-color: #ff3300; padding: 6px; color: #fff;">   
                              <div class="col-md-4"><b>Item Name</b></div> 
                              <div class="col-md-3"><b>Price</b></div> 
                              <div class="col-md-3"><b>QTY</b></div> 
                              <div class="col-md-2"><b></b></div> 
                          </div>
                      </div>
                  </div>
                  <?php 
                    foreach($cartItems as $value){
                     $itemCartId = $value['id'];
                     $itemCartName = $value['item_name'];
                     $quant = $value->getQuantity();
                      ?>
                     <div class="row" style="background-color: #fff; padding: 10px;">     
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                          <div class="row"> 
                              <div class="col-md-4" style="padding: 5px;"><?=$value['item_name'];?></div> 
                              <div class="col-md-3" style="padding: 5px;">₹ <?= $quant*($value['price']);?></div>
                              <div class="col-md-3 wid" style="border: 1px solid; border-radius: 24px; padding: 5px;">
                                  <span style="padding-left: 5px; cursor: pointer;"><i class="fa fa-minus decrementcart" id="dec_<?=$itemCartId;?>"></i></span>
                                  <span id="mgQuant_<?= $itemCartId;?>"><?= $value->getQuantity();?></span>
                                  <span class="mrg" style="padding-right: 10px; cursor: pointer;"><i class="fa fa-plus incrementcart" id="inc_<?=$itemCartId;?>"></i></span>
                                  <a class="btn-updateaddcart cusbutton" id="add-cart_<?= $itemCartId;?>" style="cursor: pointer;">Update</a>
                              </div>
                              <div class="col-md-2" style="padding: 5px;"> <a class="btn-remove-item" id="remove_item_<?= $value['id'];?>"><i class="fa fa-trash pull-right"></i>
                                </a></div>  
                          </div>
                      </div>
                    </div>
                      <?php } ?>
                      
                      <hr>
                      <div class="row"> 
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                        <table>
                           <?php 
                           if(!empty($userDistance)){
                           if($userDistance < 5){
                           $shipcharge = ($restaurant->shiping_charge * $userDistance);
                           $shipingcharge = ceil($shipcharge);
                           $charge = $shipingcharge + ($cart->getCost());
                           }
                           elseif($userDistance > 5)
                           {
                           $shipcharge = ($restaurant->shiping_charge_above * $userDistance);
                           $shipingcharge = ceil($shipcharge);
                           $charge = $shipingcharge + ($cart->getCost());
                           }
                           }
                           else
                           {
                           $shipcharge = $restaurant->shiping_charge ;
                           $shipingcharge = ceil($shipcharge);
                           $charge = $shipingcharge + ($cart->getCost());
                           }

                           ?>
                          <tr><td><h6>Amount: </h6></td><td><h6>&nbsp;&nbsp;&nbsp;&nbsp; ₹ <?= $cart->getCost();?></h6></td></tr>
                          <tr><td><h6>Delivery Charge: </h6></td><td><h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;₹ <span id="changePrice"><?= $shipingcharge;?></span></h6></td></tr>
                          <tr><td><h6></h6></td><td><h6></span></h6></td></tr>
                           <tr><td><h6></h6></td><td><h6></span></h6></td></tr>
                            <tr><td><h6></h6></td><td><h6></span></h6></td></tr>
                          <tr><td><h5><b>Total Amount: </b></h5></td><td><h5><b>&nbsp;&nbsp;&nbsp; ₹ <span id="changeTotal"><?= $charge;?></span></b></h5></td></tr>
                           <tr><td><h6></h6></td><td><h6></span></h6></td></tr>
                        </table>
                        </div>
                        </div>
                      
                    </div>
                    <div class="col-md-1">
                    </div>
                  </div>
                 </div>
                </section><?php }?>
           