
<?php 

use yii\helpers\Html;
use \app\helpers\ImageUploader;
//use  \app\modules\MubAdmin\modules\hotels\models\Restaurant;

use  \app\modules\MubAdmin\modules\hotels\models\RestaurantImages;
//use  \app\modules\MubAdmin\modules\hotels\models\Menu;
$where['del_status'] = '0';
$where['status'] = 'active';
if(\Yii::$app->getRequest()->getCookies()->has('cityName'))
{
    $cityName =  \Yii::$app->request->cookies->getValue('cityName');
    $where['city_name'] = $cityName;
}
$restaurant = new \app\modules\MubAdmin\modules\hotels\models\Restaurant();
$restaurantDetails = $restaurant::find()->where($where)->all();

//$item = new \app\modules\MubAdmin\modules\hotels\models\Item();
//$itemDetails = $item::find()->where(['del_status' => '0'])->limit(9)->all();
?>
<!-- //results show -->
        <div class="page-wrapper">

            <section class="inner-page-hero bg-image" style="padding-top: 15%;" data-image-src="/images/9.jpg">
                <div class="profile">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12  col-md-4 col-lg-4 profile-img">
                                <div class="image-wrap">
                                 <figure><!-- <img src="<?//= $resImgDetail->thumbnail_url;?>" alt="Profile Image"> --></figure>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 profile-desc">
                               <div class="pull-left right-text white-txt">
                                  <h6><a href="profile.html#"><?php //$resDetail->restaurant_name;?></a></h6>
                                  <!-- <a class="btn btn-small btn-green">Open</a> -->
                                  <p>
                                  </p>
                                  <!-- <ul class="nav nav-inline">
                                     <li class="nav-item"> <a class="nav-link active" href="profile.html#"><i class="fa fa-check"></i> Min Order $ 10,00</a> </li>
                                     <li class="nav-item"> <a class="nav-link" href="profile.html#"><i class="fa fa-motorcycle"></i> 30 min</a> </li>
                                     <li class="nav-item ratings">
                                        <a class="nav-link" href="profile.html#"> <span>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        </span> </a>
                                     </li>
                                  </ul> -->
<!--                                   <p><img src="/images/pin.png" style="height: 30px;">
                                  <?php //$resDetail->sa_a.$resDetail->sa_b;?></p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end:Inner page hero -->
            <div class="breadcrumb">
               <div class="container">
                  <ul>
                     <li><a href="" class="active">Home</a></li>
                     <li><a href="/site/restaurant-result/">Restaurant</a></li> <li><span class="primary-color"><strong><?=count($restaurantDetails);?></strong></span> Results so far 
                            </li>
                  </ul>

               </div>
            </div>

            <!-- <div class="result-show">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                           
                        </div>
                    </div>
                </div>
            </div> --><br/>

            <section class="restaurants-page">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <!-- Each popular food item starts -->



                                <?php 

                                    // foreach($itemDetails as $value){
                                    // $menu = Menu::findOne($value->menu_id);
                                    // $name = $menu->menu_name;
                                    // $rest = $menu->restaurant_id;
                                    // $restaurant = Restaurant::findOne($rest);
                                    // $res_name = $restaurant->restaurant_name;
                                    // $res_add = $restaurant->sa_a.','. $restaurant->sa_b;
                                    // $res_id = $restaurant->id;
                                    // $restaurantImages = RestaurantImages::findOne($res_id);
                                    // $res_img = $restaurantImages->url;

                                ?>
                                <?php 

                                    //$menuId = new Menu();

                                    //$menu = $menuId::find()->where(['del_status' => '0'])->all();

                                    //$itemPrice = $item::find()->where(['menu_id' => $menu->id])->all();

                                    foreach ($restaurantDetails as $restaurant) {
                                    $restaurantImages = RestaurantImages::find()->where(['del_status' => '0','restaurant_id' =>$restaurant->id])->one();
                                     if($restaurantImages)
                                        {

                                        $res_img = ($restaurantImages->url) ? $restaurantImages->url : 'NA';
                                        }else
                                        {
                                            $res_img = 'NA';
                                        }
                                ?>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 food-item">
                                    <div class="food-item-wrap">
                                         <a href="/site/restaurant-list?name=<?= $restaurant->restaurant_slug;?>">
                                        <div class="figure-wrap bg-image" data-image-src="<?= ImageUploader::resizeRender($res_img, '250', '175'); ?>">
                                        </div>
                                        </a>

                                       
                                            <!-- <div class="distance"><i class="fa fa-pin"></i>1240m</div>
 -->                                            <!-- div class="rating pull-left"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                            <div class="review pull-right"><a href="">98 reviews</a> </div> -->
                                        
                                        <div class="content">
                                            <h5><a href="/site/restaurant-list?name=<?=  $restaurant->restaurant_slug;?>"><?= substr($restaurant->restaurant_name, 0, 26); ?></a></h5>
                                            <div class="product-name"><?= substr($restaurant->restaurant_exerpt , 0 ,34);?><br>
                                            <!-- <div class="price-btn-block"> <span class="price">$ 15,99</span> <a href="profile.html" class="btn theme-btn-dash pull-right">Order Now</a> </div> -->
                                            <?= substr($restaurant->sa_b, 0 , 30);?>...</div>
                                        </div>
<!--                                         <div class="restaurant-block">
                                            <div class="left">
                                                <a class="pull-left" href="food_results.html#"> <img src="/images/logo1.png" alt="Restaurant logo"> </a>
                                                <div class="pull-left right-text"> <a href="food_results.html#"><span><?= substr($restaurant->sa_b, 0 , 20);?></span> </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                                <?php }?>
                                <!-- Each popular food item starts -->
                             </div>
                            <!-- end:right row -->
                        </div>
                    </div>
                </div>
            </section>
            <!-- start: FOOTER -->
        </div>