<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css">button, input, select, textarea {
    line-height: inherit;
    width: 208px!important;}
  .modal-footer {
    padding: 15px;
    border-top: 1px solid #fff;</style>
</style>


<div class="login-popup-outer container">
     <h3 class="magazines-by-sectors-heading " style="margin: 1em; text-align: center;"><strong>Forget Password</strong></h3>
        <?php $form = ActiveForm::begin(['options' => ['id' => 'frontend-forget','method' => 'POST','data-pjax' => true],'action' => ['/']]); ?>
            <div class="col-lg-12 nopadding product-advertise-form" style="margin-top: -1em;">
              <div class="col-lg-12 nopadding product-advertise-form-inner">
                <div class='col-sm-10 col-sm-offset-1' style="text-align: center; margin-left: 39px;">
               <?= $form->field($model, 'email')->textInput()->input('email', ['placeholder' => "Registered Email ID", 'class' => 'login-popup-panel-main input'])->label(false); ?></div>
              </div>
              <div class="col-xs-12 nopadding product-advertise-form-inner product-submit-button" style="padding: 1em; text-align: center">
                <input type="submit" class="btn btn-danger" name="login" value="Submit">
              </div><br>
              <div class="col-xs-12 nopadding product-advertise-form-inner memnber-yet-con" style="text-align: center; margin-bottom: 1em;"><span></span> <span>Not a member? <a href="/site/client" class="signup-user login-button-nav"> Register</a></span></div>
            </div>
         <?php ActiveForm::end(); ?>
</div>

<div class="modal-footer">  
</div>

