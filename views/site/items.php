 
<?php 
use yz\shoppingcart\ShoppingCart;
$cart = new ShoppingCart();

 foreach ($items as $item){ 
     if(($item['plate'] == 'full-plate')){
     $recomended = $item->recomended;
      if($recomended == '0' ){     

        $menu = new \app\modules\MubAdmin\modules\hotels\models\Menu();
        $selectMenu = $item['menu_id'];    
        $menuDetails = $menu::find()->where(['id' => $selectMenu,'del_status' => '0',])->one();
        $id = $menuDetails->restaurant_id;
        $restaurant = new \app\modules\MubAdmin\modules\hotels\models\Restaurant();
        $resDetail = $restaurant::find()->where(['id' => $id,'del_status' => '0'])->one();
        $name = $resDetail->restaurant_slug;
    
                                         ?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 food-item padnew">
    <div class="food-item-wrap newpad">
        
        <div class="content color2">
            <?php
                $alreadyInCart = $cart->getPositionBySlug($item->item_slug);
            ?>   
           <div class="row"><div class="col-md-5"><h5><a href="#"><?= $item->item_name;?></a></h5></div><div class="col-md-7"><h6> <?php if(!empty($item->description)){ ?> ( <?= $item->description;?> )<?php }?></h6></div></div>
            <!-- <div class="product-name"></div> -->
            <div class="price-btn-block">
                <?php if($item->food_type == 'non-veg'){ 
                                              ?>
                <div class="col-md-3" style="padding-top: 10px; padding-left: 5px"><img src="/images/non-veg.jpg" height="33"></div>
                <?php } else{?>
                <div class="col-md-3" style="padding-top: 10px; padding-left: 5px"><img src="/images/veg.png" height="33"></div>
                <?php }?> <span class="price">₹ <?= $item->price; ?></span> 
            	<a href="/site/order?name=<?= $name;?>&item=<?= $item->item_slug;?>" id="loginmodal" data-toggle="modal" data-target="#myModal"><p class="btn theme-btn-dash pull-right <?=($alreadyInCart)? 'check' :'' ?>">Order</p></a>
                                                <?php if($alreadyInCart){?>
                                        <a href="/site/checkout"><p class="btn theme-btn-dash pull-right" style="background-color: red; color: #fff;">Checkout</p></a>
                                        <?php }?>
 
            </div>
        </div>
    </div>
</div>  
<?php }}}?>   
                    