<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\ClientSignup;
$model = new ClientSignup();
?>
<style type="text/css">@media (max-width: 62em) {
    .modal-body{
        padding: 40px!important;
    }</style>

<div class="modal-body real-spa">
<div class="login-grids">
    <div class="login">
        <div class="login-right">
            <?php $form = ActiveForm::begin(['layout' => 'horizontal','enableAjaxValidation' => true,'validationUrl' => ['site/client-register-validate'],'options' => ['id' => 'frontend-signup','method' => 'POST','data-pjax' => true],'action' => ['/#']]); ?>
                <div class="col-md-12 text-center" style="margin-top: 4em"><h2>Register</h2></div>
                <div class="row"><?= $form->field($model, 'first_name')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'last_name')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'username')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'password')->passwordInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'mobile')->textInput(['class' => 'form-control','maxlength' => 10,]);?></div>
                <div class="row"><?= $form->field($model, 'email')->textInput(['class' => 'form-control']);?></div>
                <div class="row"><?= $form->field($model, 'street_address')->textInput(['class' => 'form-control'])->label('Street Address');;?></div>
                <div class="row"><?= $form->field($model, 'address')->textInput(['class' => 'form-control', 'id' => 'searchPlaces'])->label('Address');?></div>
                 <?= $form->field($model,'lat')->hiddenInput(['id' => 'lat'])->label(false);?>
                 <?= $form->field($model,'long')->hiddenInput(['id' => 'long'])->label(false);?>
                 <div class="row" style="margin-top: -3em;">
                <div class="col-md-4"></div><div class="col-md-8"><input type="submit" value="Register Now" style="margin-top: -2em!important;color: #fff; margin-bottom: 0.2em; width: 150px; background: #058a0b; padding: 5px;">&nbsp;&nbsp; Already Registered | Go to 
                <a href="/site/client-login" id="loginmodal" data-toggle="modal" data-target="#myModal" style="color: red; font-weight: 600; font-size: 17px;">Login</a></div></div>
             <?php ActiveForm::end(); ?>
        </div>
        <div class="clearfix"></div>                                
    </div>
    <div class="row">
    <div class="col-md-4"></div><div class="col-md-8"><p style="color: #000!important;">By logging in you agree to our <a href="#" style="color: #6298c7!important;" target="_blank">Terms and Conditions</a> and <a href="#" style="color: #6298c7!important;" target="_blank">Privacy Policy</a></p></div><br><br>
    </div>
</div>
</div>

