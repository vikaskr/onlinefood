     
        <section class="popular">
            <div class="container">
                <div class="title text-xs-center m-b-30">
                    <h2>Popular This Month In Your City</h2>
                    <p class="lead">The easiest way to your favourite food</p>
                </div>
                <div class="row">
                    <?php foreach($itemDetails as $value){
                      if(($value['plate'] == 'full-plate')){
                       $menu = Menu::find()->where(['del_status' => '0','id' =>$value->menu_id])->one();

                        if(!empty($menu))
                        {
                            $name = $menu->menu_name;
                            $rest = $menu->restaurant_id;
                            $restaurant = Restaurant::find()->where(['del_status' => '0','id' =>$rest])->one();
                        }
                        if(!empty($restaurant))
                        {
                            $res_name = $restaurant->restaurant_name;
                            $res_slug = $restaurant->restaurant_slug;
                            $res_del = $restaurant->delivery_time;
                            $res_cost = $restaurant->cost_for_to;
                            $res_add = $restaurant->sa_a.','. $restaurant->sa_b;
                            $res_id = $restaurant->id;
                            $restaurantImages = RestaurantImages::find()->where(['del_status' => '0','restaurant_id' =>$res_id])->one();
                            //echo $restaurantImages->createCommand()->getRawSql();
           
                        }
                        if($restaurantImages)
                        {
	                        $res_img = ($restaurantImages->url) ? $restaurantImages->url : 'NA';
                        }
			            else
                        {
                            $res_img = 'NA';
                        }
                        if($value['image_url'] == '')
                        {
                            $image = '/images/not-found.png';
                        }
                        else
                        {
                            $image = '/'.$value['image_url'];
                        }
                    ?>

                    <?php //echo Html::img(ImageUploader::resizeRender($propertyImage, '255', '166'));
                    ?>
                    <?php //echo Html::img(ImageUploader::resizeRender($propertyImage, '255', '143'),['class' => 'img-responsive','alt' => $res->sa_b]);?>
                    <?php 
                        if(!empty($restaurant))
                        {
                            if(!empty($menu))
                        {
                     ?><?php if($restaurant->status == 'active'){?>
                    <div class="col-xs-12 col-sm-6 col-md-4 food-item">
                            <div class="food-item-wrap">
                                <a href="site/restaurant-list?name=<?= $res_slug;?>">
                                    <div class="figure-wrap bg-image img__wrap" data-image-src="<?= ImageUploader::resizeRender($image, '250', '175'); ?>">
                                        <!-- <div class="distance"><i class="fa fa-pin"></i>1240m</div> -->
                                        <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-4"></div>
                                        <?php if($value['food_type'] == 'non-veg'){ 
                                        ?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 35px"><img src="/images/non-veg.jpg" height="25"></div>
                                        <?php } else{?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 35px"><img src="/images/veg.png" height="25"></div>
                                        <?php }?>
                                        </div>
                                        <?php if(!empty($value['description'])){?>
                                        <p class="img__description"><?= $value['description'];?></p>
                                        <?php } ?>
                                        <!-- <div class="rating pull-left"> <i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                        <div class="review pull-right"></div> -->
                                    </div>
                                </a>

                                <div class="content">
                                    <h5><a href="site/restaurant-list?name=<?= $res_slug;?>"><?= $value['item_name'];?></a></h5>
                                    <div class="product-name"><?= $name;?></div>
                                        <?php
                                            $alreadyInCart = $cart->getPositionBySlug($value['item_slug']);
                                        ?>
                                    <div class="price-btn-block"><span class="price">₹<?= $value['price']?></span>
                                        <a href="/site/order?name=<?= $res_slug;?>&item=<?= $value['item_slug'];?>" id="loginmodal" data-toggle="modal" data-target="#myModal"><p class="btn theme-btn-dash pull-right <?=($alreadyInCart)? 'check' :'' ?>">Order</p></a>
                                        <?php if($alreadyInCart){?>
                                        <a href="/site/checkout"><p class="btn theme-btn-dash pull-right" style="background-color: red; color: #fff;">Checkout</p></a>
                                        <?php }?>
                                    </div>
                                </div>
                                <div class="restaurant-block">
                                    <div class="left">

                                    <a class="pull-left" href="site/restaurant-list?name=<?= $res_slug;?>"> 
                                        <?php echo Html::img(ImageUploader::resizeRender($res_img, '120', '50'),['class' => 'img-responsive']);?>
                                    </a>

                                        <div class="pull-left right-text"> <a href="site/restaurant-list?name=<?= $res_slug;?>"><?= $res_name;?></a> <span><?= substr($res_add, 0 , 25);?>...</span> </div>

                                        <div class="pull-left right-text" style="margin-top: 15px!important;">  Delivery Time : <?= substr($res_del, 0 , 2);?>-min | Cost for Two: ₹ <?= $res_cost;?></div>
                                    </div>

                                </div>
                            </div>

                    </div>
                   <?php } } } } }?>
                </div>
            </div>
        </section>
        <!-- Featured restaurants ends -->
        <section class="app-section">
            <div class="app-wrap">
                <div class="container">
                    <div class="row text-img-block text-xs-left">
                        <div class="container">
                            <div class="col-xs-12 col-sm-5 right-image text-center">
                                <figure> <img src="/images/app.png" alt="Right Image" class="img-fluid"> </figure>
                            </div>
                            <div class="col-xs-12 col-sm-7 left-text">
                                <h3>The Best Food Delivery Website</h3>
                                <p>Now you can make food happen pretty much wherever you are thanks to the free easy-to-use Food Delivery &amp; Takeout Website.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>