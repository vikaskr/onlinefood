<?php

    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    use yii\widgets\ActiveForm;
    use yz\shoppingcart\ShoppingCart;
    date_default_timezone_set("UTC");
    $name = Yii::$app->getRequest()->getQueryParam('name');
    $restaurantImg = new \app\modules\MubAdmin\modules\hotels\models\RestaurantImages();

    $offers = new \app\modules\MubAdmin\modules\hotels\models\Offers();
    $restaurantOffers = $offers::find()->where(['del_status' => '0', 'status' => 'active', 'restaurant_id' => $restaurant->id])->one();

    if(!empty($menus)){
    $images = $restaurantImg::find()->where(['restaurant_id' => $menus[0]->restaurant_id,'del_status' => '0'])->all();
    $items = [];
    $itemnew = $menus[0]->items;
    foreach ($menus as $menuKey => $menu) 
    {
      foreach ($menu->items as $itemKey => $item) 
      {
        $items[] = $item;
      }
    }
    $cart = new ShoppingCart();
    $cartItems = $cart->getPositions();

    $time_now = mktime(date('H')+6,date('i')-30);
    $localTime = date("Y-m-d  H:i:s",$time_now);

    $currentHour = date('H',$time_now)-12;

        if($currentHour >= 0 && $currentHour < 12)
        {
        $current = 'PM';
        }
        else
        {
        $current = 'AM';
        }

    $currentTime = date("h:i", strtotime($localTime));
    $newCurrentTIme = $currentTime." $current";
    $openTime = $restaurant->open_time;
    $closeTime = $restaurant->close_time;
    // p(['openTime' => $openTime, 'currentTime' => $newCurrentTIme,'closeTime' => $closeTime]);

    $cTime = strtotime($newCurrentTIme);
    $oTime = strtotime($openTime);
    $clTime = strtotime($closeTime);
    // p(['openTime' => $oTime, 'currentTime' => $cTime,'closeTime' => $clTime]);
?>
<style type="text/css">
  @media (min-width: 280px) and (max-width: 680px) {
    .mart{
      margin-top: 0em!important;
    }
    .fon{
      font-size: 20px!important;
    }
  }
</style>
</style>
        <div class="page-wrapper">
            <section class="inner-page-hero bg-image bg-color" data-image-src="//images/9.jpg">
               <div class="profile">
                  <div class="container" style="margin-top: 12px!important;">
                     <div class="row">
                        <div class="col-xs-12 col-sm-12  col-md-4 col-lg-4 profile-img">
                           <div class="image-wrap">
                           <?php 
                              if($images)
                              {
                                $logoImage = $images[0]->thumbnail_url;
                                $res_img = ($images[0]->thumbnail_url) ? $images[0]->thumbnail_url : 'NA';
                              }
                              else
                              {
                                  $res_img = 'NA';
                              }
                           ?>
                              <figure><img src="<?= $res_img;?>" alt="Profile Image"></figure>
                           </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 profile-desc">
                           <div class="pull-left right-text white-txt">
                              <h6><a  style="font-weight: bold;" href=""><?= $restaurant->restaurant_name;?></a></h6>
                              <?php 
                              if ($cTime > $oTime && $cTime < $clTime){
                                ?>
                              
                              <a class="btn btn-small btn-green">Open</a>
                              <?php } else {?>
                              <a class="btn btn-small btn-green closed">Closed</a>
                              <?php }?>
                              <p><?php 
                              $count = count($menus);
                              foreach ($menus as $key => $value) 
                              {
                                $comma = ($key == ($count-1)) ? "" : ',';
                                ?>
                                <?= $value['menu_name']. $comma;?>
                              <?php }?></p>
                              <ul class="nav nav-inline">
                                 <li class="nav-item" style="color: #fff!important;"> <i class="fa fa-check"></i> Cost for Two: ₹ <?=$restaurant->cost_for_to;?></li>
                                 <li class="nav-item" style="color: #fff!important;"> <i class="fa fa-motorcycle"></i>  <?=$restaurant->delivery_time;?></li>
                                 <li class="nav-item ratings" style="color: #ffd041!important;">
                                     <span>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    </span> 
                                 </li>
                              </ul>
                              <p><!-- <img src="/images/pin.png" style="height: 30px;">  --><?= $restaurant->sa_a.' '.$restaurant->sa_b;?></p>
                           </div>
                        </div>
                        <?php if(!empty($restaurantOffers->offer_text)){?>
                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 profile-desc mart" style="margin-top: 1em; background: #dcdcdc3d; border: 2px solid #fff; padding: 10px;">
                        <h3 style="margin-left: 3.5em; margin-bottom: 1em; font-weight: 600; color: #fff;">Only For Today</h3>
                          <div class="row" style="text-align: center;"><!-- <div class="col-md-3"><img src="/images/1.jpg" height="60"></div> --> <div class="col-md-12"><span style="color: #fff; font-size: 23px;" class="fon"><?= $restaurantOffers->offer_text;?></span></div></div>
                        </div>
                        <?php }?>
                     </div>
                  </div>
               </div>
            </section>
            <!-- end:Inner page hero -->
            <div class="breadcrumb">
               <div class="container">
                    <?php 
                    $rCount = 1;
                    foreach ($items as $key => $item){
                      if(($item['plate'] == 'full-plate')){
                      $recomended = $item->recomended;
                      if($recomended == '1' ){
                        if($rCount == 9)
                        {
                          break;
                        }
                      if($item->image_url != '')
                        {

                            $image = ImageUploader::resizeRender("/".$item->image_url,277, 210);
                        }
                        else
                        {
                            $image = ImageUploader::resizeRender("/uploads/not-found.png",277, 210);
                        } 
                        if($key == 8)
                        {
                          break;
                        }
                      ?>
                      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-3 food-item">
                          <div class="food-item-wrap">
                              <div class="figure-wrap bg-image img__wrap" data-image-src="<?= $image ?>">
                                  <!-- <div class="distance"><i class="fa fa-pin"></i>1240m</div> -->
                                   <div class="row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-4"></div>
                                              
                                        <?php if($item->food_type == 'non-veg'){ 
                                              ?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 35px"><img src="/images/non-veg.jpg" height="25"></div>
                                        <?php } else{?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 35px"><img src="/images/veg.png" height="25"></div>
                                        <?php }?>
                                    </div>    
                                    
                                       <?php if(!empty($item->description)){?>
                                        <p class="img__description"><?= $item->description;?></p>
                                        <?php } ?>

                              </div>
                              <div class="content">
                                <?php
                                            $alreadyInCart = $cart->getPositionBySlug($item->item_slug);
                                            
                                        ?>
                                  <h5><a href=""><?= $item->item_name; ?></a></h5>
                                  <div class="price-btn-block">
                                    <span class="price">₹ <?= $item->price; ?></span> 
                                      <!--  <a href="/site/order?name=<?= $name;?>&item=<?= $item->item_slug;?>" id="loginmodal" data-toggle="modal" data-target="#myModal"><p class="btn theme-btn-dash pull-right" id="add-order_<?= $item->id?>">Order</p></a> -->
                                       <a href="/site/order?name=<?= $name;?>&item=<?= $item->item_slug;?>" id="loginmodal" data-toggle="modal" data-target="#myModal"><p class="btn theme-btn-dash pull-right <?=($alreadyInCart)? 'check' :'' ?>">Order</p></a>
                                        <?php if($alreadyInCart){?>
                                        <a href="/site/checkout"><p class="btn theme-btn-dash pull-right" style="background-color: red; color: #fff;">Checkout</p></a>
                                        <?php }?>
                                  </div>
                              </div>
                          </div>
                      </div>  
                        
                      <?php $rCount++;} } }?> 
               </div>
            </div>
                  <div class="container m-t-30">
               <div class="row">
                  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                     <div class="sidebar clearfix m-b-20">
                       <div class="sidebar clearfix m-b-20 newmb">
                                <div class="main-block">
                                    <form>
                                       <ul>
                                          <li class="backcolor">Restaurant Menus</li>
                                       </ul>
                                        <ul id="checkbox">

                                            <?php foreach($menus as $menu){?>
                                            <li>
                                                <label class="custom-control custom-checkbox">
                                                    <input type="checkbox" value="<?= $menu->id; ?>" name="restarunt" class="restaurant custom-control-input">
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">
                                                        <?= $menu->menu_name; ?>
                                                    </span>
                                                </label>
                                            </li>

                                            <?php } ?>

                                        </ul>
                                    </form>
                                </div>
                            </div>
                     </div><br/>
                     <!-- end:Left Sidebar -->
                  </div>
                  <div class="col-xs-12 col-sm-8 col-md-8 col-lg-6">
                     <div class="menu-widget m-b-30">
                        <div class="widget-heading backcolor">
                           <h3 class="widget-title text-dark col">
                              POPULAR ORDERS Delicious hot food! <a class="btn btn-link pull-right" data-toggle="collapse"  aria-expanded="true">
                              <i class="fa fa-angle-right pull-right"></i>
                              <i class="fa fa-angle-down pull-right"></i>
                              </a>
                           </h3>
                           <div class="clearfix"></div>
                        </div>
                         <div class="col-xs-12 col-sm-7 col-md-12 col-lg-12">
                            <div class="row" id="dynamic-div">  
                           
                              <?php foreach ($itemnew as $item){ 
                                if(($item['plate'] == 'full-plate')){
                                 $recomended = $item->recomended;
                                if($recomended == '0' ){
                                  ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 food-item padnew">
                                    <div class="food-item-wrap newpad">
                                         <?php
                                            $alreadyInCart = $cart->getPositionBySlug($item->item_slug);
                                        ?>   
                                        <div class="content color2">
                                            <div class="row"><div class="col-md-5"><h5><a href="#"><?= $item->item_name;?></a></h5></div><div class="col-md-7"><h6> <?php if(!empty($item->description)){ ?> ( <?= $item->description;?> )<?php }?></h6></div></div>
                                            <!-- <div class="product-name"></div> -->
                                            <div class="price-btn-block"><?php if($item->food_type == 'non-veg'){ 
                                              ?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 5px"><img src="/images/non-veg.jpg" height="33"></div>
                                        <?php } else{?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 5px"><img src="/images/veg.png" height="33"></div>
                                        <?php }?><span class="price">₹ <?= $item->price; ?></span>
                                               <a href="/site/order?name=<?= $name;?>&item=<?= $item->item_slug;?>" id="loginmodal" data-toggle="modal" data-target="#myModal"><p class="btn theme-btn-dash pull-right <?=($alreadyInCart)? 'check' :'' ?>">Order</p></a>
                                                 <?php if($alreadyInCart){?>
                                        <a href="/site/checkout"><p class="btn theme-btn-dash pull-right" style="background-color: red; color: #fff;">Checkout</p></a>
                                        <?php }?>
                                      </div>
                                        </div>
                                    </div>
                                </div>  
                                <?php }}}?> <br/>            
                            </div>
                        </div>
                        <!-- end:Collapse -->
                     </div>
                  </div>
                  <!-- end:Bar -->
                  <div class="col-xs-12 col-md-12 col-lg-3" id="bottomcart">
                     <div class="sidebar-wrap">
                        <div class="widget widget-cart">
                           <div class="widget-heading backcolor">
                              <h3 class="widget-title text-dark col">
                                 Your Shopping Cart
                              </h3>
                              <div class="clearfix"></div>
                           </div>
                           <div class="order-row bg-white">
                              <div class="widget-body" id="cart-items">
                              <?php           
                              $total = $cart->getCost();                   
                                  foreach($cartItems as $cartItem){
                                     $quant = $cartItem->getQuantity();
                                     ?>
                                  <div class="title-row"><b><?= $cartItem->item_name;?></b>
                                  <a class="btn-remove-item" id="remove_item_<?= $cartItem->id; ?>"><i class="fa fa-trash pull-right"></i>
                                  </a></div>
                                    <div class="form-group row no-gutter">
                                      <div class="col-xs-6">
                                        <b>₹ <?=$cartItem->price;?></b>
                                      </div>
                                      <div class="col-xs-6">
                                          <span style="padding-left: 5px; cursor: pointer;"><i class="fa fa-minus decrementcart" id="dec_<?=$cartItem->id;?>"></i></span>
                                          <span id="mgQuant_<?= $cartItem->id;?>"><?= $cartItem->getQuantity();?></span>
                                          <span style="padding-right: 10px; cursor: pointer;" class="mrg"><i class="fa fa-plus incrementcart" id="inc_<?=$cartItem->id;?>"></i></span>
                                          <a class="btn-updateaddcart cusbutton" id="add-cart_<?= $cartItem->id;?>" style="cursor: pointer;">Update</a>
                                      </div>
                                    </div>
                              <?php }?>
                              <input type="hidden" id="total-price" value="<?= $total;?>">
                              </div>
                           </div>
                           <!-- end:Order row -->
                           <div class="widget-delivery clearfix">
                              <form>
                                 <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 b-t-0">
                                    <label class="custom-control custom-radio">
                                    <input id="radio4" name="radio" type="radio" class="custom-control-input" checked=""> <span class="custom-control-indicator"></span> <span class="custom-control-description">Delivery</span> </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-12 col-md-6 col-lg-6 b-t-0">
                                     <h3 class="value" style="margin-bottom: 2px!important;"><div id="display-price">₹ <?= $cart->getCost()?></div></h3>
                                 </div><br/><br/>
                                  <div class="widget-body">
                                    <div class="price-wrap" style="text-align: center; margin-top: 2em!important;">
                                       <?php if(!empty($cart->getCost())){?>
                                       <button onclick="location.href='/site/checkout'" type="button" class="btn theme-btn btn-lg">Checkout</button><?php }?>
                                    </div>
                                 </div>
                              </form>
                           </div>
                           
                        </div>
                     </div>
                  </div>
                  <!-- end:Right Sidebar -->
               </div>
               <!-- end:row -->
            </div>

</div><?php } ?>