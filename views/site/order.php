<?php

    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use \app\helpers\ImageUploader;
    use yii\widgets\ActiveForm;
    use yz\shoppingcart\ShoppingCart;
    $cart = new ShoppingCart();
    $cartItems = $cart->getPositions();
    $name = Yii::$app->getRequest()->getQueryParam('name');
    $restaurantImg = new \app\modules\MubAdmin\modules\hotels\models\RestaurantImages();
    $rest = new \app\modules\MubAdmin\modules\hotels\models\Restaurant();
    $resId = $rest::find()->where(['restaurant_slug' => $name,'del_status' => '0'])->one();
    
    $restaurant_id = $resId->id;

    if(!empty($menus)){
    $images = $restaurantImg::find()->where(['restaurant_id' => $menus[0]->restaurant_id,'del_status' => '0'])->all();
    $items = [];
    $itemnew = $menus[0]->items;
    foreach ($menus as $menuKey => $menu) 
    {
      foreach ($menu->items as $itemKey => $item) 
      {
        $items[] = $item;
      }
    }

}
?>
 <style type="text/css">
.modal.in .modal-dialog {
  margin-top: 7em!important;
}

@media (max-width: 680px) {
.modal-content{
  height: 320px!important;
}
.add{
  margin-top: -9em!important;
}
}

input {
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;

  border-radius: 50%;
  width: 16px;
  height: 16px;

  border: 2px solid #999;
  transition: 0.2s all linear;
  outline: none;
  margin-right: 5px;

  position: relative;
  top: 2px;
}

input:checked {
  border: 6px solid green;
}

</style>
<div class="container">
	<div class="row">
		<div class="col-md-8" style="padding: 3em;">
			<h3>Item Quantity</h3><br/>
			<?php foreach ($itemAdd as $additem){ ?>
			<div class="row">
			₹ <?= $additem['price'];?>
			<input type="radio" class="with-gap" name="itemPlate" value="<?= $additem['price'];?>" id="<?= $additem['id']?>" checked="" ><label for="<?= $additem['id']?>" class="order"><?= $additem->plate; ?></label>
			</div>
			<?php } 
            $alreadyInCart = $cart->getPositionById($additem['id']);?><br/>
		</div>
		<div class="col-md-4 add" style="padding: 5em; margin-top: 2em;">
			<p class="btn theme-btn-dash pull-right btn-addcart <?=($alreadyInCart)? 'check' :'' ?>" id="add-cart_<?= $additem['id'];?>">Add</p>
           
		</div>
	</div>
</div>
<!-- <script type="text/javascript">$('#myModal').on('hidden.bs.modal', function () {
 location.reload();
})</script> -->