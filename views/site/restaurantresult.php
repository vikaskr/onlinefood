
<?php 

use yii\helpers\Html;
use \app\helpers\ImageUploader;
use yz\shoppingcart\ShoppingCart;
use \app\modules\MubAdmin\modules\hotels\models\Restaurant;
use \app\modules\MubAdmin\modules\hotels\models\RestaurantImages;
use \app\modules\MubAdmin\modules\hotels\models\Menu;

$urlData = Yii::$app->getRequest()->getQueryParam('item');
$cityName = Yii::$app->getRequest()->getQueryParam('city');
$item = new \app\modules\MubAdmin\modules\hotels\models\Item();
$itemModel = $item::find();
$where = ['item.del_status' => '0','item_name' => $urlData];
    if($cityName!=='')
    {
        $itemModel->innerJoin('menu','item.menu_id = menu.id')
        ->innerJoin('restaurant','menu.restaurant_id = restaurant.id');
        $where['restaurant.city_name'] = $cityName;
    }
    $itemDetails = $itemModel->where($where)->all();
    // p($itemModel->createCommand()->rawSql);
if(!empty($itemDetails)){
$itemDetail = $item::find()->where(['del_status' => '0','item_name' => $urlData])->one();
$count = count($itemDetails);
$cart = new ShoppingCart();
$cartItems = $cart->getPositions();
$restaurant = new \app\modules\MubAdmin\modules\hotels\models\Restaurant();
$restaurantDetails = $restaurant::find()->where(['del_status' => '0','status' => 'active'])->all();
?>
<!-- //results show -->
        <div class="page-wrapper">

            <section class="inner-page-hero bg-image" style="padding-top: 15%;" data-image-src="/images/9.jpg">
                <div class="profile">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12  col-md-4 col-lg-4 profile-img">
                                <div class="image-wrap">
                                 <figure><!-- <img src="<?//= $resImgDetail->thumbnail_url;?>" alt="Profile Image"> --></figure>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 profile-desc">
                               <div class="pull-left right-text white-txt">
                                  <h6><a href="profile.html#"><?php //$resDetail->restaurant_name;?></a></h6>
                                  <!-- <a class="btn btn-small btn-green">Open</a> -->
                                  <p>
                                  </p>
                                  <!-- <ul class="nav nav-inline">
                                     <li class="nav-item"> <a class="nav-link active" href="profile.html#"><i class="fa fa-check"></i> Min Order $ 10,00</a> </li>
                                     <li class="nav-item"> <a class="nav-link" href="profile.html#"><i class="fa fa-motorcycle"></i> 30 min</a> </li>
                                     <li class="nav-item ratings">
                                        <a class="nav-link" href="profile.html#"> <span>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                        </span> </a>
                                     </li>
                                  </ul> -->
<!--                                   <p><img src="/images/pin.png" style="height: 30px;">
                                  <?php //$resDetail->sa_a.$resDetail->sa_b;?></p> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- end:Inner page hero -->
            <div class="breadcrumb">
               <div class="container">
                  <ul>
                     <li><a href="" class="active">Home</a></li>
                     <li><a href="/site/restaurant-result/">Restaurant</a></li>

                     <li><a href="/site/restaurant-result/">Items</a></li> <li><span class="primary-color"></span> Results for <strong><?= $itemDetail->item_name;?> 
                            </li>
                  </ul>

               </div>
            </div>

            <!-- <div class="result-show">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                           
                        </div>
                    </div>
                </div>
            </div> --><br/>


            <section class="restaurants-page">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                             <div class="row">
                    <?php foreach($itemDetails as $value){
                        if($value['plate'] == 'full-plate'){
                        $features = $value->feature;
                       
                        $menu = Menu::find()->where(['del_status' => '0','id' =>$value->menu_id])->one();

                        if(!empty($menu))
                        {
                            $name = $menu->menu_name;
                            $rest = $menu->restaurant_id;
                            $restaurant = Restaurant::find()->where(['del_status' => '0','id' =>$rest])->one();
                        }
                        if(!empty($restaurant))
                        {
                            $res_name = $restaurant->restaurant_name;
                            $res_slug = $restaurant->restaurant_slug;
                            $res_del = $restaurant->delivery_time;
                            $res_cost = $restaurant->cost_for_to;
                            $res_add = $restaurant->sa_a.','. $restaurant->sa_b;
                            $res_id = $restaurant->id;
                            $restaurantImages = RestaurantImages::find()->where(['del_status' => '0','restaurant_id' =>$res_id])->one();
                            //echo $restaurantImages->createCommand()->getRawSql();
           
                        }
                        if(!empty($restaurantImages))
                        {
                            $res_img = ($restaurantImages->url) ? $restaurantImages->url : 'NA';
                        }
                        else
                        {
                            $res_img = 'NA';
                        }
                        if($value['image_url'] == '')
                        {
                            $image = '/images/not-found.png';
                        }
                        else
                        {
                            $image = '/'.$value['image_url'];
                        }
                    ?>

                    <?php //echo Html::img(ImageUploader::resizeRender($propertyImage, '255', '166'));
                    ?>
                    <?php //echo Html::img(ImageUploader::resizeRender($propertyImage, '255', '143'),['class' => 'img-responsive','alt' => $res->sa_b]);?>
                    <?php 
                        if(!empty($restaurant))
                        {
                            if(!empty($menu))
                        {
                     ?>
                    <?php if($restaurant->status == 'active'){?>
                    <div class="col-xs-12 col-sm-6 col-md-4 food-item">
                            <div class="food-item-wrap">
                                <a href="/site/restaurant-list?name=<?= $res_slug;?>">
                                    <div class="figure-wrap bg-image" data-image-src="<?= ImageUploader::resizeRender($image, '250', '175'); ?>">
                                       <!--  <div class="distance"><i class="fa fa-pin"></i>1240m</div> -->
                                        <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-4"></div>
                                        <?php if($value['food_type'] == 'non-veg'){ 
                                        ?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 35px"><img src="/images/non-veg.jpg" height="33"></div>
                                        <?php } else{?>
                                        <div class="col-md-3" style="padding-top: 10px; padding-left: 35px"><img src="/images/veg.png" height="33"></div>
                                        <?php }?>
                                        </div>
                                       <!--  <div class="rating pull-left"> <i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                        <div class="review pull-right"></div> -->
                                    </div>
                                </a>

                                    <!--  <div class="figure-wrap bg-image" data-image-src="<?//= $value['image_url']?>">
                                    <div class="distance"><i class="fa fa-pin"></i>1240m</div>
                                    <div class="rating pull-left"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> </div>
                                    <div class="review pull-right"><a href="">198 reviews</a> </div>
                                </div> -->
                                <div class="content">
                                    <h5><a href="/site/restaurant-list?name=<?= $res_slug;?>"><?= substr($value['item_name'], 0 , 35);?></a></h5>
                                   <!--  <div class="product-name"><?= $name;?></div> -->
                                        <?php
                                            $alreadyInCart = $cart->getPositionBySlug($value['item_slug']);
                                        ?>
                                    <!-- <div class="price-btn-block"><span class="price">₹<?= $value['price']?></span>  <a href="/site/order?name=<?= $res_slug;?>&item=<?= $value['item_slug'];?>" id="loginmodal" data-toggle="modal" data-target="#myModal"><p class="btn theme-btn-dash pull-right <?=($alreadyInCart)? 'check' :'' ?>">Order</p></a>
                                        <?php if($alreadyInCart){?>
                                        <a href="/site/checkout"><p class="btn theme-btn-dash pull-right" style="background-color: red; color: #fff;">Checkout</p></a>
                                        <?php }?>
                                            </div> -->
                                </div>

                                <div class="restaurant-block">
                                    <div class="left">

                                    <a class="pull-left" href="/site/restaurant-list?name=<?= $res_slug;?>"> 
                                        <?php echo Html::img(ImageUploader::resizeRender($res_img, '120', '50'),['class' => 'img-responsive']);?>
                                    </a>

                                        <div class="pull-left right-text"> <a href="/site/restaurant-list?name=<?= $res_slug;?>"><?= $res_name;?></a> <span><?= substr($res_add, 0 , 40);?>...</span> </div>

                                        <div class="pull-left right-text" style="margin-top: 15px!important;">  Delivery Time : <?= substr($res_del, 0 , 2);?>-min | Cost for Two: ₹ <?= $res_cost;?></div>
                                    </div>

                                </div>
                            </div>

                    </div>
                   <?php } } } } } ?>
                </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- start: FOOTER -->
        </div><?php } else {?>
        <div class="page-wrapper">

            <section class="inner-page-hero bg-image" style="padding-top: 15%;" data-image-src="/images/9.jpg">
                <div class="profile">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12  col-md-4 col-lg-4 profile-img">
                                <div class="image-wrap">
                                 <figure><!-- <img src="<?//= $resImgDetail->thumbnail_url;?>" alt="Profile Image"> --></figure>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 profile-desc">
                               <div class="pull-left right-text white-txt">
                                  <h6><a href="profile.html#"><?php //$resDetail->restaurant_name;?></a></h6>
                                  <!-- <a class="btn btn-small btn-green">Open</a> -->
                                  <p>
                                  </p>
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section></div></strong></li></ul></div></div></div>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4" style="padding: 3em;">
                <h1>No Item Found!</h1>
                <br>
                <h3><button><a href="/">Search Dishes Again....</a></button></h3>
                </div>
            </div>
        <?php }?>