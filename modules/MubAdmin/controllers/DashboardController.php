<?php

namespace app\modules\MubAdmin\controllers;

use Yii;
use app\components\MubController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\City;
use app\models\MubUser;
use yz\shoppingcart\ShoppingCart;
use app\models\HomesListing;
use app\components\Model;
use yii\helpers\ArrayHelper;

class DashboardController extends MubController
{
	public function getProcessModel()
	{

	}

	public function getPrimaryModel()
	{

	}

	public function getDataProviders()
	{
		return [];
	}

    public function saveBookingDetails($postData)
    {
         $cart = new ShoppingCart();
         $cartItems = $cart->getPositions();
         $price = $cart->getCost();

        if(!empty($postData))
        {
            $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking::deleteAll('booking_id = '."'".$postData['txnid']."'");
            $booking->name = $postData['firstname'];
            $booking->mub_user_id = $mubUserId;
            $booking->resturant_name = $postData['resname'];
            $booking->address = $postData['address'];
            $booking->alt_address = $postData['address3'];
            $booking->lat = $postData['lat'];
            $booking->long = $postData['long'];
            $booking->street_address = $postData['searchPlaces'];
            $booking->time = $postData['time'];
            $booking->email = $postData['email'];
            $booking->booking_id = $postData['txnid'];
            $booking->mobile = $postData['phone'];
            $booking->item = $postData['item'];
            $booking->payment_mode = 'Online Payment';
            $booking->amount = $postData['amount'];
            $booking->quantity = $postData['quant'];
            $booking->delivery_charge = $postData['delivery_charge'];
            
            $add = $booking->address;
            $add2 = $booking->alt_address;
            $res = $booking->resturant_name;
            $quant = $booking->quantity;

            if(!$booking->save())
            {
               p($booking->getErrors());
            }
            $itemName = [];
            $quant = [];
            foreach ($cartItems as $value) 
            {
                $itemName[] = $value['item_name'];
                $quant[] = $value->getQuantity();
            }

           $success = \Yii::$app->mailer->compose('Listhomemail',['cartItems' => $cartItems, 'price' => $price, 'res' => $res, 'add' => $add, 'add2' => $add2, 'quant' => $quant])
                    ->setFrom('info@mayaexpress.in')
                    ->setCc('praveen@makeubig.com')
                    ->setBcc('info@mayaexpress.in','MayaExpress ADMIN')
                    ->setTo($booking->email)->setSubject('Your Booked Order')
                    ->send();    

                return $success;
        }
        return true;
    }

    public function actionBookingCod()
    {
         $cart = new ShoppingCart();
         $cartItems = $cart->getPositions();
         $price = $cart->getCost();
         $cod = \Yii::$app->request->getBodyParams();
            $mubUserId = \app\models\User::getMubUserId();
            $booking = new \app\models\Booking();
            $booking->name = $cod['firstname'];
            $booking->mub_user_id = $mubUserId;
            $booking->resturant_name = $cod['resname'];
            $booking->address = $cod['address'];
            $booking->alt_address = $cod['address3'];
            $booking->street_address = $cod['searchPlaces'];
            $booking->lat = $cod['lat'];
            $booking->long = $cod['long'];
            $booking->time = $cod['time'];
            $booking->email = $cod['email'];
            $booking->mobile = $cod['phone'];
            $booking->item = $cod['item'];
            $booking->payment_mode = 'COD';
            $booking->amount = $cod['price'];
            $booking->quantity = $cod['quant'];
            $booking->delivery_charge = $cod['delivery_charge'];
            
            $add = $booking->address;
            $add2 = $booking->alt_address;
            $add3 = $booking->street_address;
            $res = $booking->resturant_name;
            $quant = $booking->quantity;

            if(!$booking->save())
            {
               p($booking->getErrors());
            }
            $itemName = [];
            $quant = [];
            foreach ($cartItems as $value) 
            {
                $itemName[] = $value['item_name'];
                $quant[] = $value->getQuantity();
            }
               

            $success = \Yii::$app->mailer->compose('Listhomemail',['cartItems' => $cartItems, 'price' => $price, 'res' => $res, 'add' => $add, 'add2' => $add2,'add3' => $add3,'quant' => $quant])
                    ->setFrom('info@mayaexpress.in')
                    ->setCc('praveen@makeubig.com')
                    ->setBcc('info@mayaexpress.in','MayaExpress ADMIN')
                    ->setTo($booking->email)->setSubject('Your Booked Order')
                    ->send();
                    if($success)
                    {
                        $ch = curl_init();
                        $items = implode(",",$itemName);
                        $message = "Order for $booking->resturant_name of Rs. $booking->amount With COD method";
                        $userCoded = urlencode($message);
                        $url = "http://sms4power.com/api/swsendUni.asp?username=t1mayaexpress&password=102470947&sender=MAYAEX&sendto=9736738974&message=".$userCoded;
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                        $output = curl_exec($ch); 
                        curl_close($ch); 

                        $ch = curl_init();
                        $items = implode(",",$itemName);
                        $message = "Order for $booking->resturant_name of Rs. $booking->amount With COD method. Thank you for placing order with MayaExpress. Your bill worth Rs. $booking->amount is due , please keep the cash available.";
                        $userCoded = urlencode($message);
                        $url = "http://sms4power.com/api/swsendUni.asp?username=t1mayaexpress&password=102470947&sender=MAYAEX&sendto=<?= $booking->mobile;?>&message=".$userCoded;
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                        $output = curl_exec($ch); 
                        curl_close($ch); 
                    }   
                return $success;

    }

	public function actionGetCity() {
        if (Yii::$app->request->isAjax) {
            $stateId = Yii::$app->request->post('stateId');
            // $data = [];
            $result = City::find()->where(['state_id' => $stateId])->all();
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return [
                'result' => $result
            ];
        } else {
            return false;
        }
    }

    public function actionSetAttribute()
    {
        if (Yii::$app->request->isAjax) 
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $params = \Yii::$app->request->getBodyParams();
            $model = new $params['model']();
            $dataModel = $model::findOne($params['id']);
            if($params['model'] == '\\app\\models\\User')
            {
                $mubUser = MubUser::findOne($params['id']);
                $dataModel = $model::find()->where(['id' => $mubUser->user_id,'del_status' => '0'])->one();
            }
            $dataModel->{$params['attribute']} = $params['value'];
            $response['message'] = 'Data Updated successfully';
            if(!$dataModel->save(false))
            {
                $key = array_keys($dataModel->getErrors());
                $response['message'] = $dataModel->getErrors()[$key];
            }
            return $response;
        } 
        else 
        {
            return false;
        }

    }

    public function actionListHome()
    {
        
        $formData = \Yii::$app->request->getBodyParams();
        if(isset($formData['HomesListing']))
        {
            $listHome = new \app\models\HomesListing();
            if($listHome->load($formData))
            {
                if(!$listHome->save())
                {
                    p($listHome->getErrors());
                }
                // $homelistModel = new \app\modules\MubAdmin\modules\hotels\models\Property();
                // $listhome = $homelistModel::findOne($listHome['mub_user_id']);
               
                \Yii::$app->mailer->compose('Listhomemail',['listHome' => $listHome])
                    ->setFrom('info@osmstays.com')
                    ->setCc('info@osmstays.com')
                    ->setBcc('admin@makeubig.com','MakeUBIG ADMIN')
                    ->setTo([$formData['HomesListing']['email'],$listHome->email])->setSubject('Your Listed Home')
                    ->send();       
                return $listHome->id;
            }
        }
       
        return false;
    }

    public function actionPayuHash()
    {
        if(\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
            $postData = \Yii::$app->request->getBodyParams();
            $hashVarsSeq = explode('|', $hashSequence);
            $hash_string = '';  
            foreach($hashVarsSeq as $hash_var) {
                $hash_string .= isset($postData[$hash_var]) ? $postData[$hash_var] : '';
                $hash_string .= '|';
            }
            //add merchant salt here
          $hash_string .= 'AJi6ZxOUP2';
          $result['hash'] = strtolower(hash('sha512', $hash_string));
          $this->saveBookingDetails($postData);
          return $result;
        }
    }

}