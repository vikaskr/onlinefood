<?php

namespace app\modules\MubAdmin\modules\hotels\controllers;


use app\components\MubController;
use Yii;
use app\modules\MubAdmin\modules\hotels\models\Offers;
use app\modules\MubAdmin\modules\hotels\models\OffersSearch;
use app\modules\MubAdmin\modules\hotels\models\OffersProcess;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OffersController implements the CRUD actions for Offers model.
 */
class OffersController extends MubController
{
   public function getPrimaryModel()
   {
        return new Offers();
   }

   public function getProcessModel()
   {
        return new OffersProcess();
   }

   public function getSearchModel()
   {
        return new OffersSearch();
   }
}