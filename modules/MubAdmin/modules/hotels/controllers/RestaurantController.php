<?php

namespace app\modules\MubAdmin\modules\hotels\controllers;

use app\components\MubController;
use app\modules\MubAdmin\modules\hotels\models\RestaurantSearch;
use app\modules\MubAdmin\modules\hotels\models\Restaurant;
use app\modules\MubAdmin\modules\hotels\models\RestaurantProcess;
use app\modules\MubAdmin\modules\hotels\models\RestaurantImages;
use yii\web\UploadedFile;
use \app\models\Booking;
use yii\data\Pagination;
use yii\widgets\LinkPager;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;

class RestaurantController extends MubController
{
   public function getPrimaryModel()
   {
		  return new Restaurant();
   }

   public function getProcessModel()
   {
      return new RestaurantProcess();
   }

   public function getSearchModel()
   {
      return new RestaurantSearch();
   }

    public function updateUserRecord($postData)
    {
      $userModel = new \app\models\User();
      $mubUserModel = new \app\models\MubUser();
      $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
      $mubUserContact = $mubUser->mubUserContacts;
      $user = $userModel::findOne($mubUser->user_id);
      $user->first_name = $postData['MubUser']['first_name'];
      $user->last_name = $postData['MubUser']['last_name'];
      $user->setPassword($postData['MubUser']['password']);
      $user->generateAuthKey();
      $user->generatePasswordResetToken();
      return ($user->save(false)) ? true : p($user->getErrors());
    }
  
    public function actionProfile()
    {   
        if (\Yii::$app->user->isGuest) {
            $this->redirect('/mub-admin');
        }
        else 
        {
            if(\Yii::$app->request->post())
            {
                $postData = \Yii::$app->request->post();
                $userModel = new \app\models\User();
                $mubUserModel = new \app\models\MubUser();
                $mubUser = $mubUserModel::findOne($postData['MubUser']['id']);
                $user = $userModel::findOne($mubUser->user_id);
                $mubUserContact = $mubUser->mubUserContacts;
            
                if($mubUser->load($postData) && $mubUserContact->load($postData))
                {
                    if($mubUser->save(false) && $mubUserContact->save(false))
                    {
                        $success = $this->updateUserRecord($postData);
                        if($success)
                        {
                            return $this->goBack('/mub-admin/hotels/restaurant/profile');
                        }
                    }
                }
                p([$mubUser->getErrors(),$mubUserContact->getErrors()]);
            }
            return $this->render('profile');
        }
    }

   public function actionRestaurantImage($state)
   {
    if (\Yii::$app->request->isAjax) {
      $model = new RestaurantImages();

      $imageFile = UploadedFile::getInstance($model, 'url');

      $directory = \Yii::getAlias('@app/images/properties') . DIRECTORY_SEPARATOR . \Yii::$app->request->getQueryParam('state') . DIRECTORY_SEPARATOR;
      if (!is_dir($directory)) {
          FileHelper::createDirectory($directory);
      }
      if ($imageFile) {
          $uid = uniqid(time(), true);
          $fileName = $uid . '.' . $imageFile->extension;
          $filePath = $directory . $fileName;
          $rid = \Yii::$app->getRequest()->getQueryParam('restaurant');
          if ($imageFile->saveAs($filePath)) {
              $path = '/images/properties/'.\Yii::$app->request->getQueryParam('state') . DIRECTORY_SEPARATOR.$fileName;
              $model->mub_user_id = \app\models\User::getMubUserId();
              $model->restaurant_id = ($rid == '') ? NULL :$rid;
              $model->title = $uid . '.' . $imageFile->extension;
              $model->description = $imageFile->name.'.'.$imageFile->extension;
              $model->url = $path;
              $model->thumbnail_url = $path;
              $model->full_path = $path;
              if(!$model->save())
              {
                p($model->getErrors());
              }
              
              $successPath = '/images/done.png';
              return Json::encode([
                  'files' => [
                      [
                          'name' => $fileName,
                          'size' => $imageFile->size,
                          'url' => $successPath,
                          'thumbnailUrl' => $successPath,
                          'deleteUrl' => 'image-delete?name=' . $fileName,
                          'deleteType' => 'POST',
                      ],
                  ],
              ]);
          }
      }
      return '';
    }else
    {
      return 'go away';
    }
  }

  public function actionImageDelete($name)
  {
     if (\Yii::$app->request->isAjax) {
      $stateId = \Yii::$app->request->getQueryParam('state');
      $directory = \Yii::getAlias('@app/images/properties') . DIRECTORY_SEPARATOR . $stateId . DIRECTORY_SEPARATOR;
      if (is_file($directory.$name)) 
      {
          if(unlink($directory . $name))
          {
            $restaurantImages = new \app\modules\MubAdmin\modules\hotels\models\RestaurantImages();
            $success = $restaurantImages::deleteAll(['title' => $name]);
            if($success)
            {
              return 'Image Deleted Successfully'; 
            }
          }else
          {
            return 'There was a problem deleting the Image';
          }
      }
      else
      {
        $restaurantImages = new \app\modules\MubAdmin\modules\hotels\models\RestaurantImages();
        $success = $restaurantImages::deleteAll(['title' => $name]);
        if($success)
        {
          return 'Image Deleted from Records'; 
        }
      }
      
  }else
  {
    return 'Go Awayyyy!';
  }
  }

  public function actionReport()
  {
      return $this->render('report');
  }

  public function actionResultReport($id,$startdate=null,$enddate=null)
  {
      $restaurant = new Restaurant();
      $bookingData = new Booking();
      if(!empty(Yii::$app->getRequest()->getQueryParams()))
        {
          $model = new Booking();
          if($startdate!= null){
            $date1 = $startdate;
          }
          if($enddate != null)
          {
            $date2 = $enddate.' '.'23:59:59';
          }
          if(isset($date1)&&isset($date2))
          {
            $resName = $restaurant::find()->where(['restaurant_slug' => $id])->one();
            $res = $resName->restaurant_name;
            $query = "select * from `booking` where `resturant_name` = '".$res."' and `time` >= '".$date1."' and `time` <= '".$date2."'";
            $sql = \Yii::$app->db->createCommand($query);
            $item = $sql->queryAll();
            $countQuery = count($item); 
            $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 30]);
            $query = "select * from `booking` where `status` = 'done' and `resturant_name` = '".$res."' and `time` >= '".$date1."' and `time` <= '".$date2."' LIMIT ".$pages->pageSize." OFFSET ".$pages->offset."";
            $sql = \Yii::$app->db->createCommand($query);
            $itemDetail = $sql->queryAll();
           
            return $this->render('result-report',['itemDetail' => $itemDetail,'resName' => $resName,'date1' => $date1,'date2' => $date2 ,'pages' => $pages]);
          }
        }
      $resName = $restaurant::find()->where(['restaurant_slug' => $id])->one();
      $item = $bookingData::find()->where(['del_status' => '0','resturant_name' => $resName->restaurant_name, 'status'=> 'done'])->orderBy(['id'=>SORT_DESC])->all();  
      $countQuery = count($item); 
      $pages = new Pagination(['totalCount' => $countQuery,'pageSize' => 30]);
      $itemDetail = $bookingData::find()->where(['del_status' => '0','resturant_name' => $resName->restaurant_name, 'status'=> 'done'])->orderBy(['id'=>SORT_DESC])->limit($pages->limit)->offset($pages->offset)->all();

      return $this->render('result-report',['itemDetail' => $itemDetail,'resName' => $resName,'pages' => $pages]);
  }
}
