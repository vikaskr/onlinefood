<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use dosamigos\fileupload\FileUploadUI;
use app\helpers\ImageUploader;
use dosamigos\tinymce\TinyMce;
use kartik\time\TimePicker;
$stateId = \Yii::$app->request->getQueryParam('state');
        if($stateId == '')
        {
            $stateId = \Yii::$app->request->getBodyParam('state');
        }

$restaurantImages = new app\modules\MubAdmin\modules\hotels\models\RestaurantImages();
$images = $restaurantImages::find()->where(['restaurant_id' => $restaurant->id,'del_status' => '0'])->all();

/* @var $this yii\web\View */
/* @var $restaurant app\modules\MubAdmin\modules\hotels\restaurants\restaurant */
/* @var $form yii\widgets\ActiveForm */

$state = new \app\models\State();
$stateName = \app\helpers\StringHelper::generateSlug($state::findOne($stateId)->state_name);


?>

<style>
    .toggle
    {
        display:none;
    }
</style>
<?php if(!empty($allServices)){?>

<div class="property-form">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <br/><h4>Upload Imgaes</h4><br/>
        <?= FileUploadUI::widget([
        'model' => $restaurantImages,
        'attribute' => 'url',
        'url' => ['restaurant/restaurant-image', 'state' => \Yii::$app->request->getQueryParam('state'),'restaurant' => $restaurant->id],
        'gallery' => false,
        'fieldOptions' => [
            'accept' => 'image/*'
        ],
        'clientOptions' => [
            'maxFileSize' => 5000000
        ],
        'clientEvents' => [
            'fileuploaddone' => 'function(e, data) {
                                    console.log(e);
                                    console.log(data);
                                }',
            'fileuploadfail' => 'function(e, data) {
                                    console.log(e);
                                    console.log(data);
                                }',
            ],
        ]); ?>
        </div>
    </div>
    <?php $form = ActiveForm::begin(); ?>

    <?php if(!empty($images)){?>
    <?php  foreach($images as $key=>$image){?>
    <div id="uploaded<?php echo $key;?>" style="display: inline; margin-right: 10px;">
    <?php echo Html::img(ImageUploader::resizeRender($image->url, '100', '100'),['height'=>100,'width'=>100]);
    echo Html::a(
        Yii::t('app', 'x'),
        '#',
        ["class" => "btn btn-danger del-image","id" => $image->title]);
    ?>
     </div>
     <?php }}?>
     <div class="row">
    
    <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">

    <?= $form->field($restaurant, 'restaurant_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-12">
    <?= $form->field($restaurant, 'restaurant_type')->dropDownList([ 'veg' => 'Veg', 'non-veg' => 'Non-Veg', 'both' => 'Both'], ['prompt' => 'Select restaurant Type']) ?>
    </div></div>
    <!-- <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-1">
    </div> -->
    <?= $form->field($restaurant,'lat')->hiddenInput(['id' => 'lat'])->label(false);?>
    
    <?= $form->field($restaurant,'long')->hiddenInput(['id' => 'long'])->label(false);?>
    
    <?= $form->field($restaurant,'city_name')->hiddenInput(['id' => 'city_name'])->label(false);?>
    
    <?= $form->field($restaurant,'pincode')->hiddenInput(['id' => 'pincode'])->label(false);?>
    
    <?= $form->field($restaurant,'locality_name')->hiddenInput(['id' => 'locality'])->label(false);?>
   <div class="row">
    <div class="col-md-5 col-sm-5 col-xs-12 col-md-offset-1">
    <?= $form->field($restaurant, 'sa_a')->textInput(['maxlength' => true,'placeholder' => 'Like plot Number/Khasra number'])->label('Street address Line1');?>
    </div>
    <div class="col-md-5 col-sm-5 col-xs-12">
    <?= $form->field($restaurant, 'sa_b')->textInput(['maxlength' => true,'placeholder'=> 'Sector / Locality name'])->label('Street address Line2'); ?></div>
    </div>

    <div class="row">
    <div class="col-md-5 col-sm-5 col-xs-12 col-md-offset-1">
    <?= $form->field($restaurant, 'mobile')->textInput(['maxlength' => true,'placeholder' => '01234567789'])->label('Mobile Number');?>
    </div>
    <div class="col-md-5 col-sm-5 col-xs-12">
    <?= $form->field($restaurant, 'telephone')->textInput(['maxlength' => true,'placeholder'=> '01234567789'])->label('Telephone Number'); ?></div>
    </div>

     <div class="row">
    <div class="col-md-5 col-sm-5 col-xs-12 col-md-offset-1">
    <?= $form->field($restaurant, 'email')->textInput(['maxlength' => true,'placeholder' => 'Enter Your Email'])->label('Email ID');?>
    </div>
    <div class="col-md-5 col-sm-5 col-xs-12">
    <?= $form->field($restaurant, 'website_url')->textInput(['maxlength' => true,'placeholder'=> 'Website Url'])->label('WebSite Url'); ?></div>
    </div>


    <div class="row">
    <div class="col-md-5 col-sm-5 col-xs-12 col-md-offset-1">
    <?= $form->field($restaurant, 'restaurant_exerpt')->textInput(['maxlength' => true,'placeholder' => 'Resturant Exerpt'])->label('Resturant Exerpt');?>
    </div>
<div class="col-md-5 col-sm-5 col-xs-12">
    <?= $form->field($restaurant, 'open_time')->widget(TimePicker::className(),  
     [
        'readonly' => true,                     
        'pluginOptions' => [
                'minuteStep' => 5,
                'showMeridian' => true,
                'defaultTime' => date('H:i', strtotime('2 hour')),
        ],
        'options'=>[
            'class'=>'form-control',
        ],
    ]); ?>
</div></div>
<div class="row">

<div class="col-md-5 col-sm-5 col-xs-12 col-md-offset-1">
    <?= $form->field($restaurant, 'close_time')->widget(TimePicker::className(),  
     [
        'readonly' => true,                     
        'pluginOptions' => [
                'minuteStep' => 5,
                'showMeridian' => true,
                'defaultTime' => date('H:i', strtotime('2 hour')),
        ],
        'options'=>[
            'class'=>'form-control',
        ],
    ]); ?>
</div>
<div class="col-md-5 col-sm-5 col-xs-12">
<?= $form->field($restaurant, 'cost_for_to')->textInput(['maxlength' => true,'placeholder' => 'Cost for 2'])->label('Cost for 2');?>
</div></div>
<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
<?= $form->field($restaurant, 'delivery_time')->textInput(['maxlength' => true,'placeholder' => 'Delivery Time'])->label('Delivery Time');?>
</div>

    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">

    <?= $form->field($restaurant, 'description')->widget(TinyMce::className(), [
    'options' => ['rows' => 6],
    'language' => 'en_GB',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        ]
    ]);?>
    </div>
    </div>
     <div class="row">
    <input type="hidden" id="current-state-name" value="<?=$stateName;?>">
     <div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-1">
    <?php echo $form->field($restaurantServices, 'service_id')->checkboxList($allServices)->label('Services'); ?></div>
     <div class="col-md-5 col-sm-6 col-xs-12">
    <?= $form->field($restaurant, 'commision')->textInput(['placeholder' => 'Commision Percentage'])->label('Commision Percentage');?>
     </div>
   <!--  <hr style="border: 1px solid;" /> -->
    </div>
    <div class="row">
        <div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-1">
            <?= $form->field($restaurant, 'shiping_charge')->textInput(['maxlength' => true,'placeholder' => 'Shiping Cost Below 5 km( Price )'])->label('Shiping Cost Below 5 km');?>
        </div>
        <div class="col-md-5 col-sm-6 col-xs-12">
            <?= $form->field($restaurant, 'shiping_charge_above')->textInput(['maxlength' => true,'placeholder' => 'Shiping Cost Above 5 km (  Price )'])->label('Shiping Cost Above 5 km');?>
        </div>
    </div> 
    </div>
    <br/><br/>
    <div class="form-group" style="text-align: center;" >
        <?= Html::submitButton($restaurant->isNewRecord ? 'Create' : 'Update', ['class' => $restaurant->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php if(!$restaurant->isNewRecord){?>
        <?= Html::a('Cancel', ['/mub-admin/hotels/restaurant','state' => $stateId], ['class' => 'btn btn-warning cancel'])?>
         <?php }?><br/><br/><br/><br/>
    </div>
</div>

<input type="hidden" id="current-state-id" value="<?=$stateId;?>">
    <?php ActiveForm::end(); ?>
    <?php }else{?><br/><br/><br/>
<h3>Please Create Some Services first to associate your Products with</h3>
<br /><br />
<h4><--- You can click on the Services option from Left Menu to create new Services</h4>

<?php }?>

</div>

