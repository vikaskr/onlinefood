<?php
use \app\models\MubUser;
use \app\models\Booking;
use \yii\helpers\Html;
use app\modules\MubAdmin\modules\hotels\models\Restaurant;
use app\modules\MubAdmin\modules\hotels\models\RestaurantSearch;
use yii\jui\DatePicker;
use yii\data\Pagination;
use yii\widgets\LinkPager;
$userRole = \Yii::$app->controller->getUserRole();

$restaurant = new Restaurant();
$bookingData = new Booking();
$id = Yii::$app->getRequest()->getQueryParam('id');
if(!empty($date1)){
   $sdate = $date1;
   $edate = $date2;
}

?>
<style type="text/css">th{
      padding: 10px;
    font-size: 12px;
    width: 12%
    }
    td{
      padding: 15px;
    font-size: 14px;
    width: 14%
}
.btn-success{
    padding: 4px!important;
}
</style>

<div class="container">
  <div class="row">
    <div class="col-md-12" style="padding: 30px; background-color: #fff;">
      <div class="row" style="border: 2px solid; padding: 10px; background: #f1f1f1;">  
       <div class="col-md-3"> 
        <h3><b><?= $resName->restaurant_name;?></b></h3>
       </div>
       <div class="col-md-7" style="margin-top:7px;"> 
        <form id="Report-date" method="GET">
            <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
            <input type="hidden" name="id" value="<?= $id;?>" />
            <span style="font-size: 18px;"><i class="fa fa-calendar" aria-hidden="true"></i> Start Date </span><?= DatePicker::widget(['name' => 'startdate', 'id' =>'date1', 'dateFormat' => 'php:Y-m-d','clientOptions' => [        'dateFormat' => 'Y-m-d',
                            'showAnim'=>'fold',
                            'changeMonth'=> true,
                            'changeYear'=> false,
                            'value' => '',
                            'autoSize'=>true,
                             'showOn'=> "button",
                            ]]); ?>
            <span style="font-size: 18px;"><i class="fa fa-calendar" aria-hidden="true"></i> End Date </span><?= DatePicker::widget(['name' => 'enddate', 'id' =>'date2', 'dateFormat' => 'php:Y-m-d', 'clientOptions' => ['dateFormat' => 'Y-m-d',
                            'showAnim'=>'fold',
                            'changeMonth'=> true,
                            'changeYear'=> false,
                            'value' => '',
                            'autoSize'=>true,
                             'showOn'=> "button",
                            ]]) ?>
          <button class="btn btn-success" id="sortdate" style="padding: 6px!important;">Apply</button>
          </form>          
       </div>
       <div class="col-md-2"> 
        <a href="/site/successpdf?name=<?= $resName->restaurant_slug;?><?php if(!empty($sdate)){?>&startdate=<?= $sdate;?>&enddate=<?= $edate;}?>" class="btn btn-success" style="background-color: red; border: 1px solid red; padding-right: 20px!important; margin-left: 10px!important; padding-left: 20px!important;" target="_blank"><h2 style="font-size: 15px;"><b>Download Pdf</b></h2></a>
       </div>
      </div><br/>

      <div class="row" style="border:2px solid; background: #ffdddd;">
       <div style="border-right: 1px solid black;" class="col-md-1"><h2 style="color: #000;">Id</h2></div>
       <div style="border-right: 1px solid black;" class="col-md-2"><h2 style="color: #000;">Restaurant Name</h2></div>
       <div style="border-right: 1px solid black;" class="col-md-2"><h2 style="color: #000;">Item</h2></div>
       <div style="border-right: 1px solid black;" class="col-md-3"><h2 style="color: #000;">Restaurant Address</h2></div>
       <div style="border-right: 1px solid black;" class="col-md-2"><h2 style="color: #000;">Date</h2></div>
       <div style="border-right: 1px solid black;" class="col-md-1"><h2 style="color: #000;">Amount</h2></div>
       <?php 
      if($userRole == 'admin' && empty($date1)){?> <div style="border-right: 1px solid black;" class="col-md-1"><h2 style="color: #000;">Delivery</h2></div><?php }?>
      </div>
       <br/>
        <?php 
        foreach ($itemDetail as $value) {
          $date = substr($value['time'], 0 , -10);
          $amount = (($value['amount']) - ($value['delivery_charge']));
        ?>
       <div class="row" style="border:1px solid; background: #f7f7f7;">
       <div style="border-right: 1px solid black;" class="col-md-1"><h5 style="color: #000;"><?= $value['id'];?></h5></div>
       <div style="border-right: 1px solid black;" class="col-md-2"><h5 style="color: #000;"><?= $value['resturant_name'];?></h5></div>
       <div style="border-right: 1px solid black;" class="col-md-2"><h5 style="color: #000;"><?= $value['item'];?></h5></div>
       <div style="border-right: 1px solid black;" class="col-md-3"><h5 style="color: #000;"><?= $resName->sa_b;?></h5></div>
       <div style="border-right: 1px solid black;" class="col-md-2"><h5 style="color: #000;"><?= $date;?></h5></div>
       <div style="border-right: 1px solid black;" class="col-md-1"><h5 style="color: #000;"> ₹ <?= $amount;?></h5></div>
        <?php 
        if($userRole == 'admin' && empty($date1)){?><div style="border-right: 1px solid black;" class="col-md-1"><h5 style="color: #000;"> ₹ <?= $value['delivery_charge'];?></h5></div><?php }?>
       </div>


       <div class="row">
       <div class="col-md-2"></div>
       <div class="col-md-8">
        </div>
          </div>

        <?php }?>
        <center><?= LinkPager::widget([
                 'pagination' => $pages,
                 ]);
                ?>  
        </center>   
            <?php
            $sum = 0;
            $sums = 0;
            foreach($itemDetail as $item) {
                $sum += (($item['amount']) - ($item['delivery_charge']));
                $sums += $item['delivery_charge'];
            }
            
            ?>  
       <div class="row" style="margin-top: 1em;">  
       <div class="col-md-8"> 
        
       </div>
       <div class="col-md-2"> 
        <h3>Total Amount :</h3>
       </div>
       <div class="col-md-2"> 
        <button class="btn btn-success" style="min-width: 77%!important; margin-left: 47px!important; padding-right: 20px!important; padding-left: 20px!important;"><h2> ₹ <?= $sum;?></h2></button>
       </div>
     </div>
     <?php 
      if($userRole == 'admin'){?>
     <div class="row" style="margin-top: 1em;">  
       <div class="col-md-7"> 
        
       </div>
       <div class="col-md-3"> 
        <h3>Total Service Charge :</h3>
        <?php $commision = (($resName->commision) / 100) * $sum;?>
       </div>
       <div class="col-md-2"> 
        <button class="btn btn-success" style="min-width: 77%!important; margin-left: 47px!important; padding-right: 20px!important; padding-left: 20px!important;"><h2> ₹ <?= $commision;?></h2></button>
       </div>
      </div>  
      <?php }?>
      <?php if($userRole == 'admin'){
        
        ?>
     <div class="row" style="margin-top: 1em;">  
       <div class="col-md-7"> 
        
       </div>
       <div class="col-md-3"> 
        <h3 style="padding-left:50px;"> Total Payable:</h3>
       </div>
       <div class="col-md-2"> 
        <button class="btn btn-success" style="min-width: 77%!important; margin-left: 47px!important; padding-right: 20px!important; padding-left: 20px!important;"><h2> ₹ <?= intval($sum - $commision);?></h2></button>
       </div>
      </div>  
      <?php }?>
      </div>
    </div>
  </div>
</div>


