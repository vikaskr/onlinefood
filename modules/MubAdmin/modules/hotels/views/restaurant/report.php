<?php
use \app\models\MubUser;
use \app\models\Booking;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Breadcrumbs;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\hotels\models\Restaurant;
use app\modules\MubAdmin\modules\hotels\models\RestaurantSearch;

$bookingData = new Booking();
$restaurant = new Restaurant();
$searchModel = new RestaurantSearch();

$mubUserId = \app\models\User::getMubUserId();
$stateId = \Yii::$app->request->getQueryParam('state');
$query = Restaurant::find()->where(['del_status' => '0']);

if($mubUserId != '1')
{
    $query->andWhere(['mub_user_id' => $mubUserId]);
}


$dataProvider = new ActiveDataProvider([
    'query' => $query,
]); 
$resId = $restaurant::find()->where(['del_status' => '0','status' => 'active','mub_user_id' => $mubUserId])->one();

$date = date('Y-m-d',strtotime("-1 days"));     
$date1 = $date;
$date2 = $date.' '.'23:59:59';
$query = "SELECT * from `booking` WHERE (`time` BETWEEN '".$date1."' and '".$date2."')";

$sql = \Yii::$app->db->createCommand($query);
$items = $sql->queryAll();
$sum = 0;

$userRole = \Yii::$app->controller->getUserRole();

if($userRole == 'subadmin'){
foreach($items as $item) 
{
    if($item['mub_user_id'] !== '1')
    {
    $sum += ($item['amount']) - ($item['delivery_charge']);
    }
} 
}

elseif($userRole == 'admin')
{
    foreach($items as $item) 
    {
        $sum += ($item['amount']) - ($item['delivery_charge']);
    }
}


?>  
<div class="container">
  <div class="row">
    <div class="col-md-12" style="border-radius: 20px; padding: 20px; background-color: #fff; border : 2px solid;">
 <button class="btn btn-success" style=" padding-right: 20px!important;margin-bottom: 2em; border-radius: 28px; padding-left: 20px!important;"><h2 style="font-size: 22px!important; ">Yesterday Revenue : ₹ <?= $sum;?></h2></button>
       <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{summary}\n{items}\n<div align='center'>{pager}</div>",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'restaurant_name',
            'sa_a',
            'sa_b',
            'city_name',
             [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    $userRole = \Yii::$app->controller->getUserRole();
                    if($userRole == 'admin'){
                        $modelName = str_replace('\\','\\\\',get_class($model));
                    $attrib = 'status'; 
                    $valActive = 'report';
                    $id = $model->restaurant_slug;
                    $valInactive = 'inactive';
                    return '<a class="btn btn-success" target="_blank" href="/mub-admin/hotels/restaurant/result-report?id='.$id.'">Report</a>';    
                    }
                    if($userRole == 'subadmin'){
                        $modelName = str_replace('\\','\\\\',get_class($model));
                    $attrib = 'status'; 
                    $valActive = 'report';
                    $id = $model->restaurant_slug;
                    $valInactive = 'inactive';
                    return '<a class="btn btn-success" target="_blank" href="/mub-admin/hotels/restaurant/result-report?id='.$id.'">Report</a>';    
                    }
                    else
                    {
                        return $model->status;
                    }
                    
                },
            ],
        ],
    ]); ?>
    </div>
  </div>
</div>

