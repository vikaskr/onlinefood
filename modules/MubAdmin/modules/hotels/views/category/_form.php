<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $category app\modules\MubAdmin\modules\hotels\categorys\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
  <div class="row">
      <div class="col-md-5 col-md-offset-1">
      <?= $form->field($category, 'category_name')->textInput(['maxlength' => true]) ?>
    </div>
     <div class="col-md-5 ">
      <?= $form->field($category, 'category_slug')->textInput(['maxlength' => true]) ?>
      </div>
 </div>
 <div class="row">
 	<div class="col-md-10 col-md-offset-1">
    <?= $form->field($category, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?>
</div>
</div>
<div class="row">
	<div class="col-md-5 col-md-offset-1">
    <div class="form-group">
        <?= Html::submitButton($category->isNewRecord ? 'Create' : 'Update', ['class' => $category->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
