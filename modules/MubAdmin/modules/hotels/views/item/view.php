<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\hotels\models\Item */

$this->title = $item->id;
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$menuId = \Yii::$app->request->getQueryParam('menu');
?>
<div class="item-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $item->id,'menu' => $menuId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $item->id, 'menu' => $menuId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $item,
        'attributes' => [
            'id',
            'mub_user_id',
            'menu_id',
            'item_name',
            'item_slug',
            // 'price',
            // 'category',
            'image_url:url',
            'status',
            'recomended',
            'feature',
            'del_status',
        ],
    ]) ?>

</div>
