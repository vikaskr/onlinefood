<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $item app\modules\MubAdmin\modules\hotels\items\Item */
/* @var $form yii\widgets\ActiveForm */
?>
<link href="/css/jquery.typeahead.css" rel="stylesheet">

<div class="item-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
      <div class="col-md-5 col-md-offset-1">
      <?= $form->field($item, 'item_name')->textInput() ?>
      </div>
      <div class="col-md-5">
     <?= $form->field($item, 'full_plate')->textInput()->label('Full Plate (Price)') ?>
      </div>
    </div>
   <div class="row">
      <div class="col-md-10 col-md-offset-1">
    <?= $form->field($item, 'description')->textarea(['rows' => '3']) ?>
      </div>
   </div>
   <div class="row">
      <div class="col-md-5 col-md-offset-1">
    <?= $form->field($item, 'half_plate')->textInput()->label('Half Plate (Price if Available)') ?>
    </div>
      <div class="col-md-5">
    <?= $form->field($item, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select status']) ?>
     </div>
    </div>
   <div class="row">
      <div class="col-md-5 col-md-offset-1">
      <?= $form->field($item, 'food_type')->dropDownList([ 'veg' => 'Veg', 'non-veg' => 'Non-Veg', ], ['prompt' => 'Select Food Type'])->label('Food Type'); ?>
     </div>
      <div class="col-md-5">
    
    </div>
    </div><br/>
   <div class="row">
      <div class="col-md-4 col-md-offset-1">
          <?= $form->field($item, 'image_url')->fileInput(); ?>
           <?php if(\Yii::$app->controller->action->id == 'update'){?>
          <div class="row text-center">
              <div class="col-md-6">
                  <img src="/<?= $item->image_url;?>" class="img-responsive" width="100">
              </div>
          </div>
          <?php }?>
      </div>
       <div class="col-md-3">
         <?= $form->field($item, 'feature')->checkbox(['feature'=>'Feature']) ?>
      </div>
      <div class="col-md-3">
      <?= $form->field($item, 'recomended')->checkbox(['recomended'=>'Recomended Food']) ?>
     </div>
   </div>
<div class="row">
      <div class="col-md-4 col-md-offset-1">
    <?php 
      // if(\Yii::$app->controller->action->id == 'update')
      //   {
      //     $itemId = \Yii::$app->request->getQueryParam('id');
      //    echo $form->field($itemCategory, 'item_id')->hiddenInput(['value'=> $itemId])->label(false);
      //   }
      $list = ['breakfast' => 'Breakfast', 'lunch' => 'Lunch', 'dinner' => 'Dinner'];
      echo $form->field($itemCategory, 'category')->checkboxList($list)->label('Serve Time'); 
    
    ?></div></div>
<div class="row">
      <div class="col-md-4 col-md-offset-1">
    <div class="form-group">
        <?= Html::submitButton($item->isNewRecord ? 'Create' : 'Update', ['class' => $item->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div></div></div>

    <?php ActiveForm::end(); ?>

</div>
