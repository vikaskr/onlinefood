<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\hotels\models\Menu */

$this->title = $menu->id;
$this->params['breadcrumbs'][] = ['label' => 'Menus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$stateId = \Yii::$app->request->getQueryParam('state');
$restaurantId = \Yii::$app->request->getQueryParam('restaurant');
?>

<div class="menu-view">
    <?= Html::a('Add/View Item', ['/mub-admin/hotels/item', 'restaurant' => $restaurantId,'menu' => $menu->id], ['class' => 'btn btn-info btn-lg pull-right'])?>

    <h1><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-md-5 col-md-offset-1">
    <p>
        <?= Html::a('Update', ['update', 'id' => $menu->id, 'state'=>$stateId], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $menu->id, 'state'=>$stateId], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
   </div>
</div>
 <div class="row">
    <div class="col-md-5 col-md-offset-1">
    <?= DetailView::widget([
        'model' => $menu,
        'attributes' => [
            'id',
            'mub_user_id',
            'restaurant_id',
            'menu_name',
            'menu_slug',
            'status',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>
    </div>
</div>
</div>
