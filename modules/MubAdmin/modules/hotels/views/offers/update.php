<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\hotels\models\Offers */

$this->title = 'Update Offers: ' . $offers->id;
$this->params['breadcrumbs'][] = ['label' => 'Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $offers->id, 'url' => ['view', 'id' => $offers->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="offers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $offers,
         'allRestaurant' => $allRestaurant,
    ]) ?>

</div>
