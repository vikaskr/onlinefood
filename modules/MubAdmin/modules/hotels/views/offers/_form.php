<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\MubAdmin\modules\hotels\models\Offers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="offers-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row"><div class="col-md-5 col-md-offset-1"><?= $form->field($model, 'offer_text')->textInput(['maxlength' => true,'placeholder' => 'Offers Text']) ?></div></div>

     <div class="row"><div class="col-md-5 col-md-offset-1"><?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select Status']) ?></div></div>

     <div class="row"><div class="col-md-5 col-md-offset-1"><?php echo $form->field($model, 'restaurant_id')->dropDownList($allRestaurant)->label('Select Restaurant'); ?></div></div><br>

     <div class="row"><div class="col-md-5 col-md-offset-1"><div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div></div></div>

    <?php ActiveForm::end(); ?>

</div>
