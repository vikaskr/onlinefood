<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $offers app\modules\MubAdmin\modules\hotels\models\Offers */

$this->title = $offers->id;
$this->params['breadcrumbs'][] = ['label' => 'Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="offers-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $offers->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $offers->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $offers,
        'attributes' => [
            'id',
            'mub_user_id',
            'restaurant_id',
            'offer_text',
            'status',
            'icon_url:url',
            'created_at',
            'updated_at',
            'del_status',
        ],
    ]) ?>

</div>
