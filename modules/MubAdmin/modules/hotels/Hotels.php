<?php

namespace app\modules\MubAdmin\modules\Hotels;

class Hotels extends \app\modules\MubAdmin\MubAdmin
{
    public $controllerNamespace = 'app\modules\MubAdmin\modules\hotels\controllers';

    public $defaultRoute = 'restaurant';

    public function init()
    {
       parent::init();
    }
}
