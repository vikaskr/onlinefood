<?php 

namespace app\modules\MubAdmin\modules\hotels\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;

class OffersProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $offers = new Offers();
        $restaurant = new Restaurant();
        $this->models = [
            'offers' => $offers,
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $restaurant = new Restaurant();
        $mubUserId = \app\models\User::getMubUserId();
        $condition = ['status' => 'active','del_status' => '0','mub_user_id' => $mubUserId];
        $allRestaurant = $restaurant->getAll('restaurant_name',$condition);
        return [
            'allRestaurant' => $allRestaurant
        ];
    }

    public function getSavedRestaurant($restaurant)
    {
        $restaurant = [];
        foreach ($restaurant as $key => $amen) 
        {
            $restaurant[] = $amen->restaurant_id;
        }

        return $restaurant;
    }

    public function getRelatedModels($model)
    {
        $restaurantSelected = $this->getSavedRestaurant($model->restaurant);

        if(!empty($restaurantSelected))
        {
            $restaurant = $model->restaurant[0];
            
            if(count($restaurantSelected) == '1')
            {
                $restaurant->restaurant_id = $restaurantSelected[0];
            }
            else
            {
                $restaurant->restaurant_id = $restaurantSelected;            
            }
        }

        else
        {
            $restaurant = new Restaurant();
        }

        $this->relatedModels = [
            'offers' => $model,
        ];
        return $this->relatedModels;
    }

    public function saveoffers($offers)
    {
        $userId = \app\models\User::getMubUserId();
        $offers->mub_user_id =  $userId;
        return ($offers->save()) ?$offers->id :p($offers->getErrors());
    }

    public function saveData($data = [])
    {
        if (isset($data['offers']))
            {
            try {
                    $offersId = $this->saveoffers($data['offers']);
                    if($offersId)
                    {
                        return $offersId;
                    }
                    p('Offers not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}