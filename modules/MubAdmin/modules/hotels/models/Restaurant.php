<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use app\models\MubUser;
use app\models\State;

/**
 * This is the model class for table "restaurant".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property string $locality_name
 * @property string $lat
 * @property string $long
 * @property string $city_name
 * @property integer $state_id
 * @property string $restaurant_name
 * @property string $restaurant_slug
 * @property string $restaurant_type
 * @property string $sa_a
 * @property string $sa_b
 * @property string $status
 * @property string $description
 * @property integer $pincode
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Menu[] $menus
 * @property MubUser $mubUser
 * @property State $state
 * @property RestaurantAmenities[] $restaurantAmenities
 * @property RestaurantImages[] $restaurantImages
 */
class Restaurant extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['restaurant_name', 'restaurant_type', 'restaurant_exerpt', 'description','sa_b','mobile','telephone','shiping_charge','shiping_charge_above'], 'required'],
            [['mub_user_id', 'state_id', 'pincode','commision'], 'integer'],
            [['restaurant_type', 'status', 'description', 'del_status'], 'string'],
            [['created_at', 'updated_at','shiping_charge','shiping_charge_above'], 'safe'],
            [['locality_name', 'lat', 'long', 'city_name', 'restaurant_name','restaurant_exerpt', 'restaurant_slug', 'sa_a', 'sa_b','open_time','close_time','cost_for_to','delivery_time','mobile','telephone','email','website_url'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'locality_name' => 'Locality Name',
            'lat' => 'Lat',
            'long' => 'Long',
            'city_name' => 'City Name',
            'state_id' => 'State ID',
            'restaurant_name' => 'Restaurant Name',
            'restaurant_exerpt' => 'Restaurant Exerpt',
            'restaurant_slug' => 'Restaurant Slug',
            'open_time' => 'Open Time',
            'close_time' => 'Close Time',
            'cost_for_to' => 'Cost For 2',
            'delivery_time' => 'Delivery Time',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'telephone' => 'Telephone',
            'website_url' => 'Website Url',
            'shiping_charge' => 'shiping_charge',
            'shiping_charge_above' => 'shiping_charge_above',
            'restaurant_type' => 'Restaurant Type',
            'sa_a' => 'Sa A',
            'sa_b' => 'Sa B',
            'status' => 'Status',
            'description' => 'Description',
            'pincode' => 'Pincode',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['restaurant_id' => 'id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id'])->where(['del_status' => '0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantServices()
    {
        return $this->hasMany(RestaurantServices::className(), ['restaurant_id' => 'id'])->where(['del_status' => '0']);
    }

    public function getResturantItems()
    {
        return $this->hasMany(ResturantItem::className(), ['resturant_id' => 'id'])->where(['del_status' => '0']);
    }    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurantImages()
    {
        return $this->hasMany(RestaurantImages::className(), ['restaurant_id' => 'id'])->where(['del_status' => '0']);
    }

}
