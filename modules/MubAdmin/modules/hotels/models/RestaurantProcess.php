<?php 

namespace app\modules\MubAdmin\modules\hotels\models;
use app\components\Model;
use app\helpers\HtmlHelper;
use Yii;

class RestaurantProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {
        $restaurant = new Restaurant();
        $restaurantServices = new RestaurantServices();
        $this->models = [
            'restaurant' => $restaurant,
            'restaurantServices' => $restaurantServices,
        ];
        return $this->models;
    }

    public function getFormData()
    {
        $services = new Service();
        $condition = ['status' => 'active'];
        $allServices = $services->getAll('service_name',$condition);
        return [
            'allServices' => $allServices
        ];
    }

    public function getSavedServices($service)
    {
        $services = [];
        foreach ($service as $key => $amen) 
        {
            $services[] = $amen->service_id;
        }

        return $services;
    }

    public function getSavedImages($image)
    {
        $images = [];
        foreach ($image as $key => $img) 
        {
            $images[] = $img->url;
        }
        return $images;
    }

    public function getRelatedModels($model)
    {
        $restaurantSelectedServices = $this->getSavedServices($model->restaurantServices);

        if(!empty($restaurantSelectedServices))
        {
            $restaurantServices = $model->restaurantServices[0];
            
            if(count($restaurantSelectedServices) == '1')
            {
                $restaurantServices->service_id = $restaurantSelectedServices[0];
            }
            else
            {
                $restaurantServices->service_id = $restaurantSelectedServices;            
            }
        }
        else
        {
            $restaurantServices = new RestaurantServices();
        }

        $this->relatedModels = [
            'restaurant' => $model,
            'restaurantServices' => $restaurantServices
        ];
        return $this->relatedModels;
    }

    public function saveRestaurant($restaurant)
    {
        if(\Yii::$app->controller->action->id =='create')
        {
            $userId = \app\models\User::getMubUserId();
        }
        else
        {
            $userId = $restaurant->mub_user_id;
        }
        $restaurant->mub_user_id = $userId;
        $restaurant->restaurant_slug = \app\helpers\StringHelper::generateSlug($restaurant->restaurant_name);
        $stateId = \Yii::$app->request->getQueryParam('state');
        $restaurant->state_id = $stateId;
        return ($restaurant->save()) ? $restaurant->id : p($restaurant->getErrors());
    }

    public function saveRestaurantServices($restaurantServices,$restaurantId)
    {
        $selectedServices = $restaurantServices->service_id;
        $mubUserId = \app\models\User::getMubUserId();
        $attribs = ['restaurant_id','service_id','mub_user_id','created_at'];
        $recordSet = [];
        foreach ($selectedServices as $serviceId) {
            $recordSet[] = [$restaurantId,$serviceId,$mubUserId,date('Y-m-d h:m:s',time())];
        }
        $amenCount = count($selectedServices);

        $restaurantServices->deleteAll(['restaurant_id' => $restaurantId]);
        $inserted = Yii::$app->db->createCommand()->batchInsert($restaurantServices->tableName(), $attribs, $recordSet)->execute();
        if($inserted == $amenCount)
        {
            return true;
        }
        p($restaurantServices->getErrors());
    }

    public function saveRestaurantImages($restaurantId)
    {   
        $restaurantImagesModel = new RestaurantImages();
        $currentUserId = \app\models\User::getMubUserId();
        $restaurantImages = $restaurantImagesModel::find()->where(['restaurant_id' => NULL, 'mub_user_id' => $currentUserId])->all();
        foreach($restaurantImages as $image)
        {   

            $image->restaurant_id = $restaurantId;
            if(!$image->save())
            {
                p($image->getErrors());
            }
        }
        return true;
    }

    public function saveData($data = [])
    {
        if (isset($data['restaurant'])&&
            isset($data['restaurantServices']))
            {
            try {
                   $restaurantId = $this->saveRestaurant($data['restaurant']);
                   if($restaurantId)
                    {
                        
                        $this->saveRestaurantImages($restaurantId);
                        $services = $this->saveRestaurantServices($data['restaurantServices'],$restaurantId);
                        if($services)
                        {
                            return $restaurantId;
                        }
                        p($data['restaurantServices']->getErrors());
                    }
                    p($data['property']->getErrors());
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}