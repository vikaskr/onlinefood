<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use app\models\MubUser;
use app\models\State;
/**
 * This is the model class for table "offers".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $restaurant_id
 * @property string $offer_text
 * @property string $status
 * @property string $icon_url
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property MubUser $mubUser
 * @property Restaurant $restaurant
 */
class Offers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'offers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offer_text','restaurant_id'], 'required'],
            [['status', 'del_status'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['offer_text', 'icon_url'], 'string', 'max' => 255],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'restaurant_id' => 'Restaurant ID',
            'offer_text' => 'Offer Text',
            'status' => 'Status',
            'icon_url' => 'Icon Url',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurant_id']);
    }
}
