<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\MubAdmin\modules\hotels\models\Offers;

/**
 * OffersSearch represents the model behind the search form about `app\modules\MubAdmin\modules\hotels\models\Offers`.
 */
class OffersSearch extends Offers
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'mub_user_id', 'restaurant_id'], 'integer'],
            [['offer_text', 'status', 'icon_url', 'created_at', 'updated_at', 'del_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $role = \app\models\User::getLoggedInUserRole();
        $where = [];
        $where['del_status'] = 0;

        if($role =='subadmin')
        {
            $mubUserId = \app\models\User::getMubUserId();
            $where['mub_user_id'] =  $mubUserId;
        }
        $query = Offers::find()->where($where);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'mub_user_id' => $this->mub_user_id,
            'restaurant_id' => $this->restaurant_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'offer_text', $this->offer_text])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'icon_url', $this->icon_url])
            ->andFilterWhere(['like', 'del_status', $this->del_status]);

        return $dataProvider;
    }
}
