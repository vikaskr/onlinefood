<?php

namespace app\modules\MubAdmin\modules\hotels\models;
use Yii;

use app\models\MubUser;

/**
 * This is the model class for table "restaurant_services_10".
 *
 * @Restaurant integer $id
 * @Restaurant integer $mub_user_id
 * @Restaurant integer $restaurant_id
 * @Restaurant integer $service_id
 * @Restaurant string $created_at
 * @Restaurant string $updated_at
 * @Restaurant string $del_status
 *
 * @Restaurant Amenity $service
 * @Restaurant MubUser $mubUser
 * @Restaurant Restaurant10 $Restaurant
 */
class RestaurantServices extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restaurant_services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mub_user_id', 'restaurant_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['del_status'], 'string'],
            [['service_id'],'required'],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
            [['restaurant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['restaurant_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'restaurant_id' => 'Restaurant ID',
            'service_id' => 'Service ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRestaurant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'restaurant_id']);
    }
}
