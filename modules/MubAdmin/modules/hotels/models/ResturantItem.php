<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;

/**
 * This is the model class for table "resturant_item".
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $resturant_id
 *
 * @property Item $item
 * @property Restaurant $resturant
 */
class ResturantItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resturant_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'resturant_id'], 'required'],
            [['item_id', 'resturant_id'], 'integer'],
            [['item_id'], 'exist', 'skipOnError' => true, 'targetClass' => Item::className(), 'targetAttribute' => ['item_id' => 'id']],
            [['resturant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Restaurant::className(), 'targetAttribute' => ['resturant_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'resturant_id' => 'Resturant ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResturant()
    {
        return $this->hasOne(Restaurant::className(), ['id' => 'resturant_id']);
    }
}
