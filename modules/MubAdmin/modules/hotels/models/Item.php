<?php

namespace app\modules\MubAdmin\modules\hotels\models;

use Yii;
use app\models\MubUser;
use yz\shoppingcart\CartPositionTrait;
use yz\shoppingcart\CartPositionInterface;
/**
 * This is the model class for table "item".
 *
 * @property integer $id
 * @property integer $mub_user_id
 * @property integer $menu_id
 * @property string $item_name
 * @property string $item_slug
 * @property integer $price
 * @property integer $sell_price
 * @property integer $discount_price
 * @property string $category_name
 * @property string $image_url
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $del_status
 *
 * @property Allcategory[] $allcategories
 * @property Menu $menu
 * @property MubUser $mubUser
 */
class Item extends \app\components\Model implements CartPositionInterface
{
    public $full_plate;
    public $half_plate;
    
    use CartPositionTrait;

    public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }

    public static function tableName()
    {
        return 'item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name','full_plate'], 'required'],
            [['mub_user_id', 'menu_id', 'price','full_plate','half_plate'], 'integer'],
            [['status', 'food_type', 'del_status'], 'string'],
            [['feature','recomended'], 'boolean'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_name', 'item_slug','plate','description'], 'string', 'max' => 255],
            [['image_url'], 'file', 'extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png','maxFiles' => 10],
            [['menu_id'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::className(), 'targetAttribute' => ['menu_id' => 'id']],
            [['mub_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => MubUser::className(), 'targetAttribute' => ['mub_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mub_user_id' => 'Mub User ID',
            'menu_id' => 'Menu ID',
            'item_name' => 'Item Name',
            'item_slug' => 'Item Slug',
            'description' => 'Description',
            'plate' => 'Plate',
            'food_type' => 'Fodd Type',
            'price' => 'Price',
            'image_url' => 'Image Url',
            'feature' => 'Feature',
            'recomended' => 'Recomended',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'del_status' => 'Del Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
   

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenu()
    {
        return $this->hasOne(Menu::className(), ['id' => 'menu_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMubUser()
    {
        return $this->hasOne(MubUser::className(), ['id' => 'mub_user_id']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemCategory()
    {
        return $this->hasMany(ItemCategory::className(), ['item_id' => 'id']);
    }
}
