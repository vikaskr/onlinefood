<?php 

namespace app\modules\MubAdmin\modules\hotels\models;
use app\components\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use app\helpers\HtmlHelper;
use app\helpers\StringHelper;
use yii\helpers\ArrayHelper;
use app\modules\MubAdmin\modules\hotels\models\Item;
use app\modules\MubAdmin\modules\hotels\models\ResturantItem;
use yii;

class ItemProcess extends Model
{
    public $models = [];
    public $deps = [];
    public $relatedModels = [];
    
    public function getModels()
    {   
        $item = new Item();
        $itemCategory = new ItemCategory();
        $this->models = [
            'item' => $item,
            'itemCategory' => $itemCategory,
        ];
        return $this->models;
    }

    public function getFormData()
    {
        return[];
    }

    public function getSavedCategory($cat)
    {   
        $category = [];
        foreach ($cat as $key => $amen) 
        {
            $category[] = $amen->item_id;
        }
        return $category;
    }
    public function getRelatedModels($model)
    {   
        $selectedCategory = $this->getSavedCategory($model->itemCategory);
        $itemModel = new Item();

        if(!empty($selectedCategory))
        {
            $itemCategory = $model->itemCategory[0];
            if(count($selectedCategory) == '1')
            {
                $itemCategory->item_id = $selectedCategory[0];
            }
            else
            {
                $itemCategory->item_id = $selectedCategory;
            }
        }
        else
        {
            $itemCategory = new ItemCategory();
        }

        $itemPlates = $itemModel::find()->where(['del_status' => '0', 'item_slug' => $model->item_slug])->all();
        foreach ($itemPlates as $key => $value) {
           if($value->plate == 'full-plate'){
            $model->full_plate = $value->price;
           }
           else{
            $model->half_plate = $value->price;
           }
        }
        
        $this->relatedModels = [
            'item' => $model,
            'itemCategory' => $itemCategory
        ];
        return $this->relatedModels;
    }

    public function saveItemCategory($itemCategory,$itemId)
    {
        if(is_array($itemId))
        {
           foreach ($itemId as $item) 
            {
                $selectedCategory = $itemCategory->category;
                $attribs = ['item_id','category','created_at'];
                $recordSet = [];
                foreach ($selectedCategory as $category) {
                    $recordSet[] = [$item,$category,date('Y-m-d h:m:s',time())];
                }
                $amenCount = count($selectedCategory);

                $itemCategory->deleteAll(['item_id' => $item]);
                $inserted = Yii::$app->db->createCommand()->batchInsert($itemCategory->tableName(), $attribs, $recordSet)->execute();
            }
            return true;
        }
        else
        {
            $selectedCategory = $itemCategory->category;
            $attribs = ['item_id','category','created_at'];
            $recordSet = [];
            foreach ($selectedCategory as $category) {
                $recordSet[] = [$itemId,$category,date('Y-m-d h:m:s',time())];
            }
            $amenCount = count($selectedCategory);

            $itemCategory->deleteAll(['item_id' => $itemId]);
            $inserted = Yii::$app->db->createCommand()->batchInsert($itemCategory->tableName(), $attribs, $recordSet)->execute();
            if($inserted == $amenCount)
            {
                return true;
            }
            p($itemCategory->getErrors());
        }
    }

    public function saveImage($item)
    {
        $item->image_url = UploadedFile::getInstance($item,'image_url');
        $imageHelper = new \app\helpers\ImageUploader();
        $success = $imageHelper::uploadImages($item,'image_url');
        return $item;
    }

    public function saveItem($item)
    {
        
        $image = UploadedFile::getInstance($item,'image_url');
        if($image)
        {
            $this->SaveImage($item);
        }
        else 
        {
            if(\Yii::$app->controller->action->id == 'update')
            {    
                $itemModel = new Item();
                $image = $itemModel->findOne($item->id)->image_url;
                $item->image_url = $image;
            }
        }   
        $menu = \Yii::$app->request->getQueryParam('menu');
        $userId = \app\models\User::getMubUserId();
           
        if($item->full_plate != $item->half_plate && $item->half_plate != "" && $item->half_plate != 0)
        {
            $prices['full-plate'] = $item->full_plate;
            $prices['half-plate'] = $item->half_plate;
            $itemModel = new Item();
            $itemSlug = StringHelper::generateSlug($item->item_name."-".$menu);
            $featured = $item->feature;
            $recomended = $item->recomended;
            $description = ($item->description=="")? NULL : $item->description;
            if(\Yii::$app->controller->action->id == 'create')
            {
            foreach ($prices as $plate => $platePrice)
            {
             $resultSet[] = [$userId,$menu,$item->item_name,$itemSlug,$featured,$recomended,$plate,$item->food_type,$platePrice,$item->image_url,$description,'active'];
            }
            $count = \Yii::$app->db->createCommand()->batchInsert('item',
            ['mub_user_id','menu_id','item_name','item_slug','feature','recomended','plate','food_type','price','image_url','description','status'], $resultSet)->execute();
            }else
            {
                $itemPlates = $itemModel::find()->where(['del_status' => '0', 'item_slug' => $item->item_slug])->all();
                foreach ($itemPlates as $key => $value) {
                   if(($value->plate == 'full-plate')){
                        $value->price = $item->full_plate;
                        $value->item_name = $item->item_name;
                        $value->item_slug = $item->item_slug;
                        $value->description = $item->description;
                        $value->food_type = $item->food_type;
                        $value->recomended = $item->recomended;
                        $value->feature = $item->feature;
                        $value->image_url = $item->image_url;
                        if(!$value->save(false))
                        {
                            p($value->getErrors());
                        }
                   }
                   elseif($value->plate == 'half-plate')
                   {
                    $value->price = $item->half_plate;
                     $value->item_name = $item->item_name;
                        $value->item_slug = $item->item_slug;
                        $value->description = $item->description;
                        $value->food_type = $item->food_type;
                        $value->recomended = $item->recomended;
                        $value->feature = $item->feature;
                        $value->image_url = $item->image_url;
                    if(!$value->save(false))
                        {
                            p($value->getErrors());
                        }
                   }
                }             
            }

            $insertedItems[] = $itemModel->find()->orderBy(['id' =>SORT_DESC])->limit(2)->all();
            return $insertedItems;
        }
        else
        {
        $item->mub_user_id =  $userId;
        $item->menu_id = $menu;
        $item->price = $item->full_plate;
        $item->item_slug = StringHelper::generateSlug($item->item_name);
        return ($item->save()) ?$item->id :p($item->getErrors());
        }
    }


     public function saveData($data = [])
     {
        if (isset($data['item'])&&
           isset($data['itemCategory']))
            {
            try {
                    $itemId = $this->saveItem($data['item']);
                    $insertedItems = []; 
                    if(is_array($itemId))
                    {
                        foreach ($itemId[0] as $itemKey => $item) 
                        {
                            $insertedItems[] = $item->id;
                        }
                    }
                    if($itemId)
                    {
                        if(is_array($itemId))
                        {
                            $itemCategory = $this->saveItemCategory($data['itemCategory'],$insertedItems);
                            $itemId = $insertedItems[0];
                        }
                        else
                        {
                            $itemCategory = $this->saveItemCategory($data['itemCategory'],$itemId);
                        }

                        if($itemCategory)
                        {
                            return $itemId;
                        }
                        p($data['itemCategory']->getErrors());
                    }
                    p('item not saved');
                }
                catch (\Exception $e)
                {
                    throw $e;
                }
            } 
            else
            {
                throw new \yii\web\HttpException(500, 'Model Not Loaded properly');
            }
    }
}