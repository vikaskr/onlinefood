<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MubUserAlbum */

$this->title = Yii::t('app', 'Create Mub User Album');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Mub User Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mub-user-album-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>