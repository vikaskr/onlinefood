<?php
use \app\models\MubUser;
use \app\models\Booking;
use app\modules\MubAdmin\modules\hotels\models\Restaurant;
$restaurant = new Restaurant();
?>
<style type="text/css">th{
      padding: 15px;
    font-size: 15px;
    width: 12%
    }
    td{
      padding: 15px;
    font-size: 14px;
    width: 14%
}</style>
<div class="container" style="border: 2px solid; border-radius: 20px;">
<div class="row" style="padding: 20px;">
<div class="x_panel">
  <div class="">
    <h2 style="margin-top: 5px; margin-bottom: -20px;"><?php if(Yii::$app->user->identity)
        {
            $mubUserId = \app\models\User::getMubUserId();                        
            $mubUser = MubUser::findOne($mubUserId); echo $mubUser->first_name?>
            <?=$mubUser->last_name;               
        }else{echo 'Guest';}
        ?></h2>
    <ul class="nav navbar-right panel_toolbox">
      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
      </li>
      <li><a class="close-link"><i class="fa fa-close"></i></a>
      </li>
    </ul>
  </div></div> <?php 
    $curId = \Yii::$app->user->id;
    $bookingData = new Booking();
    $mubUsers = new MubUser();
    $mub = $mubUsers::find()->where(['del_status' => '0','user_id' => $curId])->one(); 
    ?>
<div class="container" style="background-color: #fff;">
  <div class="row" style="padding: 20px;">
    <h1>Order</h1>
    <table border="2" width="100%"><tr><th>No.</th><th>Restutant Name</th><th> Name</th><th>Mobile No.</th><th>Order Item</th><th>Price</th><th>Payment</th><th>Address</th><th>Date & Time</th><th>Status</th> <?php if($mub->role == 'admin'){?><th>Delete</th><?php }?></tr>
   <?php
    if($mub->role == 'admin')
    {   
    $itemDetail = $bookingData::find()->where(['del_status' => '0'])->orderBy(['id'=>SORT_DESC])->all();  
    }
    elseif($mub->role == 'subadmin'){
      $restaurantsOwned = Restaurant::find()->where(['mub_user_id' => $mub->id,'del_status'=>'0'])->all();
      if(intval(count($restaurantsOwned))==1)
      {
       $itemDetail = $bookingData::find()
       ->where(['del_status' => '0'])
       ->andWhere(['like','resturant_name',$restaurantsOwned[0]->restaurant_name])
       ->orderBy(['id'=>SORT_DESC])
       ->all(); 
      }else
      {
        //TBD handle multile restaurants
        $itemDetail = [];
      }
    }
    foreach ($itemDetail as $value) {

      if((!empty($value['booking_id'])) && $value['status'] !== 'cancelled')
        { 
         $payment =  "PAID";
        } 
        else
        {
          $payment =  "COD";
        }
      if($value['status'] !== 'cancelled'){
      $resname = $value['resturant_name'];
    ?>
    <tr><td><?= $value['id'];?></td><td><?= $resname;?></td><td> <?= $value['name'];?></td><td><?= $value['mobile'];?></td><td><?= $value['item'];?></td><td><?= $value['amount'];?></td><td><?= $payment;?></td><td><?php if(!empty($value['alt_address'])) { echo $value['alt_address'].' , '.$value['street_address'];} else { echo $value['address'];}?></td><td><?= $value['time'];?></td>
      <?php if($value['status'] == 'done'){?>
      <td><button class="btn btn-success">Done</button><?php } else if($value['status'] == 'booking') { ?>
      <td><button class="btn btn-danger confirm_id" id="confirm-order" value="<?= $value['id'];?>">Approved</button></td>
      <?php }?>
      <?php if($mub->role == 'admin'){?><td><button class="btn btn-danger delete_id" id="order-delete" value="<?= $value['id'];?>">Delete</button></td><?php }?>
    </tr><?php }}?>
    </table>
  </div>
</div>
</div>
</div>
